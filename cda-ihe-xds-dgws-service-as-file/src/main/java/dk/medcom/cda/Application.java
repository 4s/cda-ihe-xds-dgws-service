package dk.medcom.cda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;

import dk.medcom.cda.configuration.ApplicationConfiguration;
import dk.medcom.cda.configuration.CredentialVaultConfiguration;
import dk.medcom.cda.configuration.DgwsConfiguration;
import dk.medcom.cda.document.DocumentHelperFromFileImpl;
import dk.medcom.cda.exceptions.ParameterException;
import dk.medcom.cda.exceptions.XdsException;
import dk.medcom.cda.services.DocumentProcessor;
import dk.medcom.cda.services.ParameterHelperImpl;
import dk.medcom.cda.services.PropertiesList;

//Source must be as copy of cda-ihe-xds-dgws-service-as-file-container.Application

@Import({ApplicationConfiguration.class,  DgwsConfiguration.class, 	CredentialVaultConfiguration.class})
@EnableAutoConfiguration
public class Application implements CommandLineRunner {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

	@Autowired
	DocumentProcessor documentProcessor;
	
	@Autowired
	PropertiesList propertiesList;
	
	public static void main(String[] args) throws Exception {
		LOGGER.debug("Starting application");
		SpringApplicationBuilder sab = new SpringApplicationBuilder(Application.class);
		sab.web(WebApplicationType.NONE);
		sab.run(args);
	}	

	public void run(String... args) throws Exception {

		try {
			ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, args);
			parameterHelper.validate();
			documentProcessor.runCDADocument(parameterHelper.getFunctionCode(), new DocumentHelperFromFileImpl(parameterHelper));
			
		} catch (XdsException e) {
			LOGGER.error(e.getMessage(), e);
			for (String error : e.getErrors()) {
				LOGGER.error(error);	
			}
			LOGGER.info("Ending application with status code 1");
			System.exit(1);
		} catch (ParameterException e) {
			LOGGER.error(e.getMessage(), e);
			LOGGER.info("Ending application with status code 1");
			System.exit(1);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e); 
			LOGGER.info("Ending application with status code 1");
			System.exit(1);
		}
		LOGGER.info("Ending application with status code 0");
		System.exit(0);	

	}
	

}
