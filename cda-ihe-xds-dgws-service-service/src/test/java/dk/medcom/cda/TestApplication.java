package dk.medcom.cda;

import java.io.File;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;

import dk.medcom.cda.configuration.ApplicationConfiguration;
import dk.medcom.cda.configuration.DgwsConfiguration;
import dk.medcom.cda.configuration.TestCredentialVaultConfiguration;
import dk.medcom.cda.document.DocumentHelperFromFileImpl;
import dk.medcom.cda.exceptions.ParameterException;
import dk.medcom.cda.exceptions.XdsException;
import dk.medcom.cda.services.DocumentProcessor;
import dk.medcom.cda.services.ParameterHelperImpl;
import dk.medcom.cda.services.PropertiesList;


@Import({ApplicationConfiguration.class,  DgwsConfiguration.class, TestCredentialVaultConfiguration.class})
@EnableAutoConfiguration
public class TestApplication implements CommandLineRunner {


	private static final Logger LOGGER = LoggerFactory.getLogger(TestApplication.class);

	@Autowired
	private PropertiesList propertiesList;
	
	@Autowired
	private DocumentProcessor documentProcessor;
	
	private String entryUuidPrefix = "urn:uuid:";

	public static void main(String[] args) throws Exception {
		LOGGER.debug("Starting application");
		SpringApplicationBuilder sab = new SpringApplicationBuilder(TestApplication.class);
		sab.web(WebApplicationType.NONE);
		sab.run(args);
	}	

	public void run(String... args) throws Exception {
//		2512489996
		
		//When running use below documentNames but remember to make the id unique first within the file
		String filePathCreate = getFilePath("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		String filePathUpdate = getFilePath("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Textv2.xml");
//		String filePathCreate = getFilePath("QRD_Example_3.0_Text_Question_adj.xml");
//		String filePathUpdate = getFilePath("QRD_Example_3.0_Text_Question_adj_v2.xml");
		
		String entryUuidCreate = entryUuidPrefix + java.util.UUID.randomUUID().toString();
		String entryUuidUpdate = entryUuidPrefix + java.util.UUID.randomUUID().toString();
		
		try {
			ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "CREATE", filePathCreate, entryUuidCreate);
			parameterHelper.validate();
			documentProcessor.runCDADocument(parameterHelper.getFunctionCode(), new DocumentHelperFromFileImpl(parameterHelper));
			
			parameterHelper = new ParameterHelperImpl(propertiesList, "REPLACE", filePathUpdate, entryUuidCreate, entryUuidUpdate);
			parameterHelper.validate();
			documentProcessor.runCDADocument(parameterHelper.getFunctionCode(), new DocumentHelperFromFileImpl(parameterHelper));
			
			parameterHelper = new ParameterHelperImpl(propertiesList, "DEPRECATE", entryUuidUpdate, "2512489996");
			parameterHelper.validate();
			documentProcessor.runCDADocument(parameterHelper.getFunctionCode(), new DocumentHelperFromFileImpl(parameterHelper));
			
			
		} catch (XdsException e) {
			LOGGER.error(e.getMessage(), e);
			for (String error : e.getErrors()) {
				LOGGER.error(error);	
			}
			LOGGER.info("Ending application with status code 1");
			System.exit(1);
		} catch (ParameterException e) {
			LOGGER.error(e.getMessage(), e);
			LOGGER.info("Ending application with status code 1");
			System.exit(1);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e); 
			LOGGER.info("Ending application with status code 1");
			System.exit(1);
		}
		LOGGER.info("Ending application with status code 0");
		System.exit(0);

	}
	
	private String getFilePath(String fileName) {
		URL url = this.getClass().getClassLoader().getResource(fileName);
		File file = new File(url.getFile()); //required on windows os
		String filePath = file.getAbsolutePath();
		return filePath;
		
	}

}
