package dk.medcom.cda.configuration;

import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import dk.sosi.seal.vault.ClasspathCredentialVault;
import dk.sosi.seal.vault.CredentialVault;
import dk.sosi.seal.vault.CredentialVaultException;

public class TestCredentialVaultConfiguration {
	
	@Value("${keystore.filename}")
	private String keystoreFilename;

	@Value("${keystore.password}")
	private String keystorePassword;
	
	@Value("${keystore.alias}")
	private String keystoreAlias;
	
	@Bean
	public CredentialVault getVault() throws CredentialVaultException, IOException {
		System.setProperty("dk.sosi.seal.vault.CredentialVault#Alias", keystoreAlias);
		return new ClasspathCredentialVault(new Properties(), keystoreFilename, keystorePassword);
	}

}
