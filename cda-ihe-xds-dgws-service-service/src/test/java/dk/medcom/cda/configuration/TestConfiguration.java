package dk.medcom.cda.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import dk.medcom.cda.services.PropertiesList;

@Configuration
@PropertySource("application.properties")
public class TestConfiguration {
	
	@Bean
	public PropertiesList propertiesList() {
		return new PropertiesList();
	}

}
