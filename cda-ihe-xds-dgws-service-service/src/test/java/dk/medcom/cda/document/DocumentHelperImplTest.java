package dk.medcom.cda.document;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import dk.medcom.cda.configuration.ApplicationConfiguration;
import dk.medcom.cda.configuration.TestConfiguration;
import dk.medcom.cda.dto.CdaMetadata;
import dk.medcom.cda.exceptions.ParameterException;
import dk.medcom.cda.services.ParameterHelperImpl;
import dk.medcom.cda.services.PropertiesList;

@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource("application.properties")
@ContextConfiguration(
		  classes = { ApplicationConfiguration.class, TestConfiguration.class}, 
		  loader = AnnotationConfigContextLoader.class)
public class DocumentHelperImplTest {
	
	@Autowired
	PropertiesList propertiesList;
	
	@Test
	public void testConstructor() throws ParameterException {
		
		// Given
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "CREATE", file.getAbsolutePath());
		
		// When
		DocumentHelper documentHelper = new DocumentHelperFromFileImpl(parameterHelper);
		
		// Then
		assertNotNull(documentHelper);
		
	}
	
	@Test
	public void testCreateDocumentAsXML() throws IOException, ParameterException {
		// Given
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "CREATE", file.getAbsolutePath());
		DocumentHelper documentHelper = new DocumentHelperFromFileImpl(parameterHelper);
		
		// When
		String document = documentHelper.createDocumentAsXML();
		
		// Then
		assertNotNull(document);
		assertTrue(document.length() > 0);
		
	}
	
	@Test
	public void testCreateCdaMetadata() throws ParameterException {
		
		// Given
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "CREATE", file.getAbsolutePath());
		DocumentHelper documentHelper = new DocumentHelperFromFileImpl(parameterHelper);
		
		// When
		CdaMetadata cdaMetadata = documentHelper.createCdaMetadata();
		
		// Then
		assertNotNull(cdaMetadata);
		assertNotNull(cdaMetadata.getAvailabilityStatus());
		assertNotNull(cdaMetadata.getObjectType());
		assertNotNull(cdaMetadata.getClassCode());
		assertNotNull(cdaMetadata.getClassCode().getCode());
		assertNotNull(cdaMetadata.getFormatCode());
		assertNotNull(cdaMetadata.getFormatCode().getCode());
		assertNotNull(cdaMetadata.getHealthcareFacilityTypeCode());
		assertNotNull(cdaMetadata.getHealthcareFacilityTypeCode().getCode());
		assertNotNull(cdaMetadata.getPracticeSettingCode());
		assertNotNull(cdaMetadata.getPracticeSettingCode().getCode());
		assertNotNull(cdaMetadata.getSubmissionTime());
		assertNotNull(cdaMetadata.getHomeCommunityId());
		
	}

}
