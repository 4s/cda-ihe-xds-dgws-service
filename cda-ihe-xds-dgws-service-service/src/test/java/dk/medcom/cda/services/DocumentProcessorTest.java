package dk.medcom.cda.services;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import dk.medcom.cda.configuration.ApplicationConfiguration;
import dk.medcom.cda.document.DocumentHelperFromFileImpl;
import dk.medcom.cda.dto.CdaMetadata;
import dk.medcom.cda.exceptions.XdsException;
import dk.medcom.cda.services.CdaMetaDataFactory;
import dk.medcom.cda.services.DocumentProcessor;
import dk.medcom.cda.services.ParameterHelper;
import dk.medcom.cda.services.XdsRequestService;

@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource("application.properties")
@ContextConfiguration(
		  classes = { ApplicationConfiguration.class}, 
		  loader = AnnotationConfigContextLoader.class)
public class DocumentProcessorTest {
	
	@Test
	public void testDocumentFlowCreate() throws IOException, XdsException {
		
		// Given
		XdsRequestService xdsRequestService = Mockito.mock(XdsRequestService.class);
		CdaMetaDataFactory cdaMetaDataFactory = Mockito.mock(CdaMetaDataFactory.class);
		DocumentHelperFromFileImpl documentHelperFromFileImpl = Mockito.mock(DocumentHelperFromFileImpl.class);
		
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		String documentContent = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())),StandardCharsets.UTF_8);
		CdaMetadata cdaMetaData = new CdaMetadata();
		String documentId = "documentId";
		
		Mockito.when(documentHelperFromFileImpl.createDocumentAsXML()).thenReturn(documentContent);
		Mockito.when(documentHelperFromFileImpl.createCdaMetadata()).thenReturn(cdaMetaData);
		Mockito.when(xdsRequestService.createAndRegisterDocument(Mockito.anyString(), Mockito.any(), Mockito.any(), Mockito.isNull())).thenReturn(documentId);
		
		DocumentProcessor documentProcessor = new DocumentProcessor(xdsRequestService, cdaMetaDataFactory);
		
		// When
		documentProcessor.runCDADocument(ParameterHelper.FunctionCode.CREATE, documentHelperFromFileImpl);
		
		// Then
		//no exceptions

	}
	
	@Test
	public void testDocumentFlowReplace() throws IOException, XdsException {
		
		// Given
		XdsRequestService xdsRequestService = Mockito.mock(XdsRequestService.class);
		CdaMetaDataFactory cdaMetaDataFactory = Mockito.mock(CdaMetaDataFactory.class);
		DocumentHelperFromFileImpl documentHelperFromFileImpl = Mockito.mock(DocumentHelperFromFileImpl.class);
		
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		String documentContent = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())),StandardCharsets.UTF_8);
		CdaMetadata cdaMetaData = new CdaMetadata();
		String documentId = "documentId";
		
		Mockito.when(documentHelperFromFileImpl.createDocumentAsXML()).thenReturn(documentContent);
		Mockito.when(documentHelperFromFileImpl.createCdaMetadata()).thenReturn(cdaMetaData);
		Mockito.when(xdsRequestService.createAndRegisterDocument(Mockito.anyString(), Mockito.any(), Mockito.any(), Mockito.anyString())).thenReturn(documentId);
		
		DocumentProcessor documentProcessor = new DocumentProcessor(xdsRequestService, cdaMetaDataFactory);
		
		// When
		documentProcessor.runCDADocument(ParameterHelper.FunctionCode.REPLACE, documentHelperFromFileImpl);
		
		// Then
		//no exceptions

	}
	
	@Test
	public void testDocumentFlowDeprecate() throws IOException, XdsException {
		
		// Given
		XdsRequestService xdsRequestService = Mockito.mock(XdsRequestService.class);
		CdaMetaDataFactory cdaMetaDataFactory = Mockito.mock(CdaMetaDataFactory.class);
		DocumentHelperFromFileImpl documentHelperFromFileImpl = Mockito.mock(DocumentHelperFromFileImpl.class);
		
		DocumentProcessor documentProcessor = new DocumentProcessor(xdsRequestService, cdaMetaDataFactory);
		
		// When
		documentProcessor.runCDADocument(ParameterHelper.FunctionCode.DEPRECATE, documentHelperFromFileImpl);
		
		// Then
		//no exceptions

	}

}
