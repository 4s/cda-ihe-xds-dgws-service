package dk.medcom.cda.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AvailabilityStatus;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntryType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import dk.medcom.cda.configuration.ApplicationConfiguration;
import dk.medcom.cda.dto.CdaMetadata;
import dk.medcom.cda.dto.Code;
import dk.medcom.cda.dto.DocumentMetadata;
import dk.medcom.cda.exceptions.ParameterException;

@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource("application.properties")
@ContextConfiguration(
		  classes = { ApplicationConfiguration.class}, 
		  loader = AnnotationConfigContextLoader.class)
public class CdaMetaDataFactoryTest {
	
	@Autowired
	CdaMetaDataFactory cdaMetaDataFactory;
	
	@Test
	public void testCreationOfMetadata() throws ParameterException, IOException {

		// Given
		CdaMetadata cdaMetaData = createCdaMetadata(new dk.medcom.cda.dto.Code("DK PHMR schema", "urn:ad:dk:medcom:phmr:full", "1.2.208.184.100.10"));

		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		String documentContent = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())),StandardCharsets.UTF_8);
		

		// When
		DocumentMetadata documentMetadata = cdaMetaDataFactory.getMetadata(cdaMetaData, documentContent);
		
		// Then
		assertNotNull(documentMetadata);
		assertEquals("2512489996", documentMetadata.getPatientId().getCode());
		assertEquals("368061000016004", documentMetadata.getOrganisation().getCode());
		assertEquals("Aalborg Universitetshospital", documentMetadata.getOrganisation().getName());
		assertEquals("1.2.208.176.1.1", documentMetadata.getOrganisation().getCodingScheme());


	}
	
	@Test
	public void testCreationOfMetadataQrdAuthorIdIsCprWithoutRepresentedOrganization() throws ParameterException, IOException {

		// Given
		CdaMetadata cdaMetaData = createCdaMetadata(new dk.medcom.cda.dto.Code("DK QRD schema", "urn:ad:dk:medcom:qrd-v1.2:full", "1.2.208.184.100.10"));

		URL url = this.getClass().getClassLoader().getResource("QRD_Example_3.0_Text_Question.xml");
		File file = new File(url.getFile());
		String documentContent = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())),StandardCharsets.UTF_8);
		

		// When
		DocumentMetadata documentMetadata = cdaMetaDataFactory.getMetadata(cdaMetaData, documentContent);
		
		// Then
		assertNotNull(documentMetadata);
		assertEquals("2512489996", documentMetadata.getPatientId().getCode());
		assertNull(documentMetadata.getOrganisation());

	}
	
	@Test
	public void testCreationOfMetadataQrdAuthorIdIsCprWithRepresentedOrganization() throws ParameterException, IOException {

		// Given
		CdaMetadata cdaMetaData = createCdaMetadata(new dk.medcom.cda.dto.Code("DK QRD schema", "urn:ad:dk:medcom:qrd-v1.2:full", "1.2.208.184.100.10"));

		URL url = this.getClass().getClassLoader().getResource("QRD_Example_3.0_Text_Question_adj.xml");
		File file = new File(url.getFile());
		String documentContent = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())),StandardCharsets.UTF_8);
		

		// When
		DocumentMetadata documentMetadata = cdaMetaDataFactory.getMetadata(cdaMetaData, documentContent);
		
		// Then
		assertNotNull(documentMetadata);
		assertEquals("2512489996", documentMetadata.getPatientId().getCode());
		assertEquals("232641000016005", documentMetadata.getOrganisation().getCode());
		assertEquals("Neurologisk Sengeafs., SLA", documentMetadata.getOrganisation().getName());
		assertEquals("1.2.208.176.1.1", documentMetadata.getOrganisation().getCodingScheme());

	}
	
	@Test
	public void testCreationOfMetadataQrdEventCodeIsAddedViaProjectRef() throws ParameterException, IOException {

		// Given
		CdaMetadata cdaMetaData = createCdaMetadata(new dk.medcom.cda.dto.Code("DK QRD schema", "urn:ad:dk:medcom:qrd-v1.2:full", "1.2.208.184.100.10"));

		URL url = this.getClass().getClassLoader().getResource("QRD_Example_3.0_Text_Question_adj.xml");
		File file = new File(url.getFile());
		String documentContent = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())),StandardCharsets.UTF_8);
		

		// When
		DocumentMetadata documentMetadata = cdaMetaDataFactory.getMetadata(cdaMetaData, documentContent);
		
		// Then
		assertNotNull(documentMetadata);
		assertEquals(1, documentMetadata.getEventCodes().size());
		assertEquals("1251f3c7-07d6-4d7d-aa89-cf33cb5cdb9f", documentMetadata.getEventCodes().get(0).getCode());
		assertEquals("Questionnaire Form Definition Document", documentMetadata.getEventCodes().get(0).getName());
		assertEquals("2.16.840.1.113883.6.1", documentMetadata.getEventCodes().get(0).getCodingScheme());


	}
	
	private CdaMetadata createCdaMetadata(Code code) {
		CdaMetadata cdaMetaData = new CdaMetadata();
		cdaMetaData.setAvailabilityStatus(AvailabilityStatus.APPROVED);
		cdaMetaData.setObjectType(DocumentEntryType.STABLE);
		cdaMetaData.setClassCode(new dk.medcom.cda.dto.Code("Klinisk rapport","001", "1.2.208.184.100.9"));
		cdaMetaData.setFormatCode(code);
		cdaMetaData.setHealthcareFacilityTypeCode(new dk.medcom.cda.dto.Code("hjemmesygepleje","550621000005101","2.16.840.1.113883.6.96"));
		cdaMetaData.setPracticeSettingCode(new dk.medcom.cda.dto.Code("almen medicin", "408443003", "2.16.840.1.113883.6.96"));
		cdaMetaData.setSubmissionTime(new Date());
		cdaMetaData.setHomeCommunityId("0.0.0");
		return cdaMetaData;
	}


}
