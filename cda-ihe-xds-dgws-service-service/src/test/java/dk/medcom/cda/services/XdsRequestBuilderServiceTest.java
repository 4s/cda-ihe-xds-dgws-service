package dk.medcom.cda.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLFactory30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLProvideAndRegisterDocumentSetRequest30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLSubmitObjectsRequest30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.ProvideAndRegisterDocumentSetRequestType;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AvailabilityStatus;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntryType;
import org.openehealth.ipf.commons.ihe.xds.core.requests.ProvideAndRegisterDocumentSet;
import org.openehealth.ipf.commons.ihe.xds.core.requests.RegisterDocumentSet;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.lcm.SubmitObjectsRequest;
import org.openehealth.ipf.commons.ihe.xds.core.transform.requests.ProvideAndRegisterDocumentSetTransformer;
import org.openehealth.ipf.commons.ihe.xds.core.transform.requests.RegisterDocumentSetTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import dk.medcom.cda.configuration.ApplicationConfiguration;
import dk.medcom.cda.document.DeprecateInformation;
import dk.medcom.cda.dto.CdaMetadata;
import dk.medcom.cda.dto.Code;
import dk.medcom.cda.dto.DocumentMetadata;


@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource("application.properties")
@ContextConfiguration(
		  classes = { ApplicationConfiguration.class}, 
		  loader = AnnotationConfigContextLoader.class)
public class XdsRequestBuilderServiceTest {
	
	@Autowired
	CdaMetaDataFactory cdaMetaDataFactory;
	
	@Test
	public void testCreateMetaData() throws IOException {
		
		// Given
		CdaMetadata cdaMetaData = new CdaMetadata();
		cdaMetaData.setAvailabilityStatus(AvailabilityStatus.APPROVED);
		cdaMetaData.setObjectType(DocumentEntryType.STABLE);
		cdaMetaData.setClassCode(new dk.medcom.cda.dto.Code("Klinisk rapport","001", "1.2.208.184.100.9"));
		cdaMetaData.setFormatCode(new dk.medcom.cda.dto.Code("DK PHMR schema", "urn:ad:dk:medcom:phmr:full", "1.2.208.184.100.10"));
		cdaMetaData.setHealthcareFacilityTypeCode(new dk.medcom.cda.dto.Code("hjemmesygepleje","550621000005101","2.16.840.1.113883.6.96"));
		cdaMetaData.setPracticeSettingCode(new dk.medcom.cda.dto.Code("almen medicin", "408443003", "2.16.840.1.113883.6.96"));
		cdaMetaData.setClassCode(new dk.medcom.cda.dto.Code("Klinisk rapport","001", "1.2.208.184.100.9"));
		cdaMetaData.setFormatCode(new dk.medcom.cda.dto.Code("DK PHMR schema", "urn:ad:dk:medcom:phmr:full", "1.2.208.184.100.10"));
		cdaMetaData.setHealthcareFacilityTypeCode(new dk.medcom.cda.dto.Code("hjemmesygepleje","550621000005101","2.16.840.1.113883.6.96"));
		cdaMetaData.setPracticeSettingCode(new dk.medcom.cda.dto.Code("almen medicin", "408443003", "2.16.840.1.113883.6.96"));
		cdaMetaData.setSubmissionTime(new Date());
		cdaMetaData.setHomeCommunityId("0.0.0");
		
		XdsRequestBuilderService xdsRequestBuilderService = new XdsRequestBuilderService();
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		
		String documentContent = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())),StandardCharsets.UTF_8);
		DocumentMetadata documentMetadata = cdaMetaDataFactory.getMetadata(cdaMetaData, documentContent);
		
		String entryUuid = "urn:uuid:efa5d2bf-06d8-4e0d-a2c1-8c8c4d736b41";
		
		// When
		ProvideAndRegisterDocumentSetRequestType provideAndRegisterDocumentSetRequest = xdsRequestBuilderService.buildProvideAndRegisterDocumentSetRequest(documentContent, documentMetadata, entryUuid, null);
		
		// Then
		assertNotNull(provideAndRegisterDocumentSetRequest);
		assertNotNull(provideAndRegisterDocumentSetRequest.getDocument());
		assertTrue(provideAndRegisterDocumentSetRequest.getDocument().size() > 0);
		assertNotNull(provideAndRegisterDocumentSetRequest.getDocument().get(0));
		assertNotNull(provideAndRegisterDocumentSetRequest.getDocument().get(0).getId());
				
		ProvideAndRegisterDocumentSet provideAndRegisterDocumentSet = revertRequest(provideAndRegisterDocumentSetRequest);
		assertNotNull(provideAndRegisterDocumentSet);
		assertEquals("da-DK", provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().getConfidentialityCodes().get(0).getDisplayName().getLang());
		assertEquals("0.0.0", provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().getHomeCommunityId());

	}
	
	@Test
	public void testBuildDeprecateSubmitObjectsRequest() throws IOException {
		
		// Given
		XdsRequestBuilderService xdsRequestBuilderService = new XdsRequestBuilderService();
		String entryUUidToDeprecate = "1.1"; 
		
		DeprecateInformation deprecateInformation = new DeprecateInformation("cpr", new Code("name", "code", "scheme"), "repositoryuniqueId", AvailabilityStatus.APPROVED, "homecommunityid");
		
		// When
		SubmitObjectsRequest submitObjectsRequest = xdsRequestBuilderService.buildDeprecateSubmitObjectsRequest(entryUUidToDeprecate, deprecateInformation);
		
		// Then
		assertNotNull(submitObjectsRequest);
		RegisterDocumentSet registerDocumentSet = revertRequest(submitObjectsRequest);
		assertNotNull(registerDocumentSet);
		assertEquals(1, registerDocumentSet.getAssociations().size());
		assertNotNull(registerDocumentSet.getSubmissionSet().getEntryUuid());
		assertNotNull(registerDocumentSet.getSubmissionSet().getLogicalUuid());
		assertEquals(entryUUidToDeprecate, registerDocumentSet.getSubmissionSet().getUniqueId());
		assertEquals("cpr", registerDocumentSet.getSubmissionSet().getPatientId().getId());
		assertEquals("repositoryuniqueId", registerDocumentSet.getSubmissionSet().getSourceId());
		assertEquals("code", registerDocumentSet.getSubmissionSet().getContentTypeCode().getCode());
		assertNotNull(registerDocumentSet.getSubmissionSet().getSubmissionTime());
		assertEquals("homecommunityid", registerDocumentSet.getSubmissionSet().getHomeCommunityId());

	}
	
	private ProvideAndRegisterDocumentSet revertRequest(ProvideAndRegisterDocumentSetRequestType provideAndRegisterDocumentSetRequest) {
		ProvideAndRegisterDocumentSetTransformer requestTransformer = new ProvideAndRegisterDocumentSetTransformer(new EbXMLFactory30());
        EbXMLProvideAndRegisterDocumentSetRequest30 ebXMLProvideAndRegisterDocumentSetRequest30 = new EbXMLProvideAndRegisterDocumentSetRequest30(provideAndRegisterDocumentSetRequest);
        ProvideAndRegisterDocumentSet provideAndRegisterDocumentSet = requestTransformer.fromEbXML(ebXMLProvideAndRegisterDocumentSetRequest30);
        return provideAndRegisterDocumentSet;
	}
	
	private RegisterDocumentSet revertRequest(SubmitObjectsRequest submitObjectsRequest) {
		RegisterDocumentSetTransformer registerDocumentSetTransformer = new RegisterDocumentSetTransformer(new EbXMLFactory30());
		EbXMLSubmitObjectsRequest30 ebXMLSubmitObjectsRequest30 = new EbXMLSubmitObjectsRequest30(submitObjectsRequest);
		RegisterDocumentSet registerDocumentSet = registerDocumentSetTransformer.fromEbXML(ebXMLSubmitObjectsRequest30);
		return registerDocumentSet;
	}

}
