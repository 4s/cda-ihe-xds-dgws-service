package dk.medcom.cda.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import dk.medcom.cda.configuration.TestConfiguration;
import dk.medcom.cda.exceptions.ParameterException;

@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource("application.properties")
@ContextConfiguration(
		  classes = { TestConfiguration.class}, 
		  loader = AnnotationConfigContextLoader.class)
public class ParameterHelperImplTest {
	
	PropertiesList propertiesList;
	String prefix = "urn:uuid:";
	String keystoreFilename = "NSP_Test_Service_Consumer_sds.p12";
	
	@Before
	public void init() {
		
		propertiesList = Mockito.mock(PropertiesList.class);
		Mockito.when(propertiesList.getXdsIti41Endpoint()).thenReturn("http://iti41");
		Mockito.when(propertiesList.getXdsIti57Endpoint()).thenReturn("http://iti57");
		Mockito.when(propertiesList.getRepositoryuniqueid()).thenReturn("1.2.208.176.43210.8.1.29");
		Mockito.when(propertiesList.getStsUrl()).thenReturn("http://test");
		Mockito.when(propertiesList.getKeystoreAlias()).thenReturn("Alias");
		Mockito.when(propertiesList.getKeystoreFilename()).thenReturn("Filename");
		Mockito.when(propertiesList.getKeystorePassword()).thenReturn("Password");
		Mockito.when(propertiesList.getCvr()).thenReturn("cvr");
		Mockito.when(propertiesList.getOrgname()).thenReturn("OrgName");
		Mockito.when(propertiesList.getItsystem()).thenReturn("ItSystem");
		
		Mockito.when(propertiesList.getClassCodeName()).thenReturn("Clinical report");
		Mockito.when(propertiesList.getClassCodeCode()).thenReturn("001");
		Mockito.when(propertiesList.getClassCodeCodingScheme()).thenReturn("1.2.208.184.100.9");
		Mockito.when(propertiesList.getFormatCodeName()).thenReturn("DK PHMR schema");
		Mockito.when(propertiesList.getFormatCodeCode()).thenReturn("urn:ad:dk:medcom:phmr:full");
		Mockito.when(propertiesList.getFormatCodeCodingScheme()).thenReturn("1.2.208.184.100.10");
		Mockito.when(propertiesList.getHealthcareFacilityTypeCodeName()).thenReturn("hjemmesygepleje");
		Mockito.when(propertiesList.getHealthcareFacilityTypeCodeCode()).thenReturn("550621000005101");
		Mockito.when(propertiesList.getHealthcareFacilityTypeCodingScheme()).thenReturn("2.16.840.1.113883.6.96");
		Mockito.when(propertiesList.getPracticeSettingCodeName()).thenReturn("almen medicin");
		Mockito.when(propertiesList.getPracticeSettingCodeCode()).thenReturn("408443003");
		Mockito.when(propertiesList.getPracticeSettingCodingScheme()).thenReturn("2.16.840.1.113883.6.96");
		Mockito.when(propertiesList.getTypeCodeName()).thenReturn("Personal Health Monitoring Report");
		Mockito.when(propertiesList.getTypeCodeCode()).thenReturn("53576-5");
		Mockito.when(propertiesList.getTypeCodeCodingScheme()).thenReturn("2.16.840.1.113883.6.1");
		Mockito.when(propertiesList.getHomeCommunityId()).thenReturn("1.2.208.176.43210.8.20");

	}
	
	@Test(expected = ParameterException.class)
	public void testInvalidFunctionCode() throws ParameterException {

		// Given
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		 
		// When
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "INVALID", file.getAbsolutePath());
		
		// Then
		// exception

	}
	
	@Test
	public void testGetEntryUuidWhenNotGiven() throws ParameterException {

		// Given
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		
		// When
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "CREATE", file.getAbsolutePath());
		
		// Then
		assertNotNull(parameterHelper.getEntryUuid());
		assertEquals(prefix, parameterHelper.getEntryUuid().substring(0, prefix.length()));

	}
	
	@Test
	public void testGetEntryUuidWhenGiven() throws ParameterException {

		// Given
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		String entryUuid = getEntryUuidWithPrefix();
		
		// When
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "CREATE", file.getAbsolutePath(), entryUuid);
		
		// Then
		assertNotNull(parameterHelper.getEntryUuid());
		assertEquals(entryUuid, parameterHelper.getEntryUuid());

	}

	
	@Test
	public void testValidateCreate() throws ParameterException {

		// Given
		URL urlDocument = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File fileDocument = new File(urlDocument.getFile());
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "CREATE", fileDocument.getAbsolutePath());
		
		URL urlProperties = this.getClass().getClassLoader().getResource(keystoreFilename);
		File fileProperties = new File(urlProperties.getFile());
		Mockito.when(propertiesList.getKeystoreFilename()).thenReturn(fileProperties.getAbsolutePath());
		
		// When
		parameterHelper.validate();
		
		// Then
		//no exceptions
	}
	
	@Test
	public void testValidateReplace() throws ParameterException {

		// Given
		URL urlDocument = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File fileDocument = new File(urlDocument.getFile());
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "REPLACE", fileDocument.getAbsolutePath(), getEntryUuidWithPrefix());
		
		URL urlProperties = this.getClass().getClassLoader().getResource(keystoreFilename);
		File fileProperties = new File(urlProperties.getFile());
		Mockito.when(propertiesList.getKeystoreFilename()).thenReturn(fileProperties.getAbsolutePath());
		
		// When
		parameterHelper.validate();
		
		// Then
		//no exceptions
	}
	
	@Test(expected = ParameterException.class)
	public void testValidateReplaceWhenEntryUuidToReplaceNotGiven() throws ParameterException {

		// Given
		URL urlDocument = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File fileDocument = new File(urlDocument.getFile());
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "REPLACE", fileDocument.getAbsolutePath());
		
		URL urlProperties = this.getClass().getClassLoader().getResource(keystoreFilename);
		File fileProperties = new File(urlProperties.getFile());
		Mockito.when(propertiesList.getKeystoreFilename()).thenReturn(fileProperties.getAbsolutePath());
		
		// When
		parameterHelper.validate();
		
		// Then
		//no exceptions
	}
	
	@Test
	public void testValidateDeprecate() throws ParameterException {

		// Given
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "DEPRECATE", getEntryUuidWithPrefix(), "cpr");
		
		URL urlProperties = this.getClass().getClassLoader().getResource(keystoreFilename);
		File fileProperties = new File(urlProperties.getFile());
		Mockito.when(propertiesList.getKeystoreFilename()).thenReturn(fileProperties.getAbsolutePath());
		
		// When
		parameterHelper.validate();
		
		// Then
		//no exceptions
	}
	
	@Test(expected = ParameterException.class)
	public void testValidateDeprecateWhenEntryUuidToDeprecateNotGiven() throws ParameterException {

		// Given
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "DEPRECATE", getEntryUuidWithPrefix());
		
		URL urlProperties = this.getClass().getClassLoader().getResource(keystoreFilename);
		File fileProperties = new File(urlProperties.getFile());
		Mockito.when(propertiesList.getKeystoreFilename()).thenReturn(fileProperties.getAbsolutePath());
		
		// When
		parameterHelper.validate();
		
		// Then
		//no exceptions
	}
	
	@Test(expected = ParameterException.class)
	public void testValidateDeprecateWhenCprToDeprecateNotGiven() throws ParameterException {

		// Given
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "DEPRECATE", "cpr");
		
		URL urlProperties = this.getClass().getClassLoader().getResource(keystoreFilename);
		File fileProperties = new File(urlProperties.getFile());
		Mockito.when(propertiesList.getKeystoreFilename()).thenReturn(fileProperties.getAbsolutePath());
		
		// When
		parameterHelper.validate();
		
		// Then
		//no exceptions
	}


	@Test(expected = ParameterException.class)
	public void testValidateCreateWhenEntryUuuidIsEmpty() throws ParameterException {

		// Given
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "CREATE", file.getAbsolutePath(), "");
		
		// When
		parameterHelper.validate();
		
		// Then
		// exception

	}
	
	@Test(expected = ParameterException.class)
	public void testValidateUuidWhenGivenWithoutPrefix() throws ParameterException {

		// Given
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		String entryUuid = getEntryUuid();
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "CREATE", file.getAbsolutePath(), entryUuid);
		
		// When
		parameterHelper.validate();
		
		// Then
		// exception

	}
	
	@Test
	public void testValidateUuidWhenGivenWithPrefix() throws ParameterException {

		// Given
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		String entryUuid = getEntryUuidWithPrefix();
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "CREATE", file.getAbsolutePath(), entryUuid);
		
		// When
		parameterHelper.validate();
		
		// Then
		// no exception
		

	}

	
	@Test
	public void testGetsCodes() throws ParameterException {

		// Given
		URL url = this.getClass().getClassLoader().getResource("PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml");
		File file = new File(url.getFile());
		
		// When
		ParameterHelperImpl parameterHelper = new ParameterHelperImpl(propertiesList, "CREATE", file.getAbsolutePath());
		
		// Then
		assertNotNull(parameterHelper.getClassCode());
		assertEquals("Clinical report", parameterHelper.getClassCode().getName());
		assertEquals("001", parameterHelper.getClassCode().getCode());
		assertEquals("1.2.208.184.100.9", parameterHelper.getClassCode().getCodingScheme());
		
		assertNotNull(parameterHelper.getFormatCode());
		assertEquals("DK PHMR schema", parameterHelper.getFormatCode().getName());
		assertEquals("urn:ad:dk:medcom:phmr:full", parameterHelper.getFormatCode().getCode());
		assertEquals("1.2.208.184.100.10", parameterHelper.getFormatCode().getCodingScheme());
		
		assertNotNull(parameterHelper.getHealthcareFacilityTypeCode());
		assertEquals("hjemmesygepleje", parameterHelper.getHealthcareFacilityTypeCode().getName());
		assertEquals("550621000005101", parameterHelper.getHealthcareFacilityTypeCode().getCode());
		assertEquals("2.16.840.1.113883.6.96", parameterHelper.getHealthcareFacilityTypeCode().getCodingScheme());
		
		assertNotNull(parameterHelper.getPracticeSettingCode());
		assertEquals("almen medicin", parameterHelper.getPracticeSettingCode().getName());
		assertEquals("408443003", parameterHelper.getPracticeSettingCode().getCode());
		assertEquals("2.16.840.1.113883.6.96", parameterHelper.getPracticeSettingCode().getCodingScheme());
		
		assertEquals("1.2.208.176.43210.8.20", parameterHelper.getHomeCommunityId());

	}
	
	
	private String getEntryUuidWithPrefix() {
		String entryUuid = java.util.UUID.randomUUID().toString();
		return prefix + entryUuid;
	}
	
	private String getEntryUuid() {
		String entryUuid = java.util.UUID.randomUUID().toString();
		return entryUuid;
	}

}
