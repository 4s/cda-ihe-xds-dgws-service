package dk.medcom.cda.services;

import java.util.Calendar;
import java.util.List;

import javax.activation.DataHandler;

import org.joda.time.DateTime;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.EbXMLFactory;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLFactory30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLProvideAndRegisterDocumentSetRequest30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLSubmitObjectsRequest30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.ProvideAndRegisterDocumentSetRequestType;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AssigningAuthority;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Association;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AssociationLabel;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AssociationType;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Author;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AvailabilityStatus;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Code;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Document;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntry;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntryType;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Identifiable;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.LocalizedString;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Name;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Organization;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.PatientInfo;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Person;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.SubmissionSet;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.XcnName;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.XpnName;
import org.openehealth.ipf.commons.ihe.xds.core.requests.ProvideAndRegisterDocumentSet;
import org.openehealth.ipf.commons.ihe.xds.core.requests.RegisterDocumentSet;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.lcm.SubmitObjectsRequest;
import org.openehealth.ipf.commons.ihe.xds.core.transform.requests.ProvideAndRegisterDocumentSetTransformer;
import org.openehealth.ipf.commons.ihe.xds.core.transform.requests.RegisterDocumentSetTransformer;

import com.sun.istack.ByteArrayDataSource;

import dk.medcom.cda.document.DeprecateInformation;
import dk.medcom.cda.dto.DocumentMetadata;
import dk.s4.hl7.cda.codes.NSI;

public class XdsRequestBuilderService {
	
	public static final String XDSSubmissionSet_contentTypeCode       = "urn:uuid:aa543740-bdda-424e-8c96-df4873be8500";

	public ProvideAndRegisterDocumentSetRequestType buildProvideAndRegisterDocumentSetRequest(String documentPayload, DocumentMetadata documentMetadata, String entryUuid, String externalIdForDocumentToReplace) {

		ProvideAndRegisterDocumentSet provideAndRegisterDocumentSet = new ProvideAndRegisterDocumentSet();

		// Create DocumentEntry for the CDA codument
		DocumentEntry documentEntry = new DocumentEntry();
		documentEntry.setEntryUuid(entryUuid);

		// Patient Identification
		Identifiable patientIdentifiable = null;
		if (documentMetadata.getPatientId() != null) {
			AssigningAuthority patientIdAssigningAuthority = new AssigningAuthority(documentMetadata.getPatientId().getCodingScheme());
			patientIdentifiable = new Identifiable(documentMetadata.getPatientId().getCode(), patientIdAssigningAuthority);
		}
		documentEntry.setPatientId(patientIdentifiable);
		
		Identifiable sourcePatientIdentifiable = null;
		if (documentMetadata.getSourcePatientId() != null) {
			AssigningAuthority patientIdAssigningAuthority = new AssigningAuthority(documentMetadata.getSourcePatientId().getCodingScheme());
			sourcePatientIdentifiable = new Identifiable(documentMetadata.getPatientId().getCode(), patientIdAssigningAuthority);
		}
		documentEntry.setSourcePatientId(sourcePatientIdentifiable);

		PatientInfo sourcePatientInfo = new PatientInfo();
		if (documentMetadata.getSourcePatientInfoPerson() != null && (documentMetadata.getSourcePatientInfoPerson().getFamilyName() != null ||documentMetadata.getSourcePatientInfoPerson().getGivenName() != null )) {
			Name<?> name = new XpnName();
			if (documentMetadata.getSourcePatientInfoPerson().getFamilyName() != null) {
				name.setFamilyName(documentMetadata.getSourcePatientInfoPerson().getFamilyName());
			}
			if (documentMetadata.getSourcePatientInfoPerson().getGivenName() != null) {
				name.setGivenName(documentMetadata.getSourcePatientInfoPerson().getGivenName());	
			}
			if (documentMetadata.getSourcePatientInfoPerson().getSecondAndFurtherGivenNames() != null) {
				name.setSecondAndFurtherGivenNames(documentMetadata.getSourcePatientInfoPerson().getSecondAndFurtherGivenNames());
			}
			sourcePatientInfo.setName(name);
		}
		
		if (documentMetadata.getSourcePatientInfoGender() != null) {
			sourcePatientInfo.setGender(documentMetadata.getSourcePatientInfoGender());	
		}
		if (documentMetadata.getSourcePatientInfoDateOfBirth() != null) {
			
			sourcePatientInfo.setDateOfBirth(documentMetadata.getSourcePatientInfoDateOfBirthString());
		}
		documentEntry.setSourcePatientInfo(sourcePatientInfo);


		// Create author (Organisation)
		Author author = new Author();
		if (documentMetadata.getOrganisation() != null && documentMetadata.getOrganisation().getCode() != null && documentMetadata.getOrganisation().getCodingScheme() != null) {
			AssigningAuthority organisationAssigningAuthority = new AssigningAuthority(documentMetadata.getOrganisation().getCodingScheme());
			Organization authorOrganisation = new Organization(documentMetadata.getOrganisation().getName(), documentMetadata.getOrganisation().getCode(), organisationAssigningAuthority);
			author.getAuthorInstitution().add(authorOrganisation);
		}
		
		//author.authorperson
		if (documentMetadata.getAuthorPerson() != null) { 
			if (documentMetadata.getAuthorPerson().getFamilyName() != null ||documentMetadata.getAuthorPerson().getGivenName() != null ) {
				Name<?> authorName = new XcnName();
				
				if (documentMetadata.getAuthorPerson().getFamilyName() != null) {
					authorName.setFamilyName(documentMetadata.getAuthorPerson().getFamilyName());	
				}
				if (documentMetadata.getAuthorPerson().getGivenName() != null) {
					authorName.setGivenName(documentMetadata.getAuthorPerson().getGivenName());	
				}
				if (documentMetadata.getAuthorPerson().getSecondAndFurtherGivenNames() != null) {
					authorName.setSecondAndFurtherGivenNames(documentMetadata.getAuthorPerson().getSecondAndFurtherGivenNames());	
				}
				Person authorPerson = new Person();
				authorPerson.setName(authorName);
				author.setAuthorPerson(authorPerson);
			}
		}
		documentEntry.setAuthor(author);
		
		//legalAuthenticator
		if (documentMetadata.getLegalAuthenticator() != null && (documentMetadata.getLegalAuthenticator().getFamilyName() != null ||documentMetadata.getLegalAuthenticator().getGivenName() != null )) {
			Name<?> legalAuthenticatorName = new XcnName();
			if (documentMetadata.getLegalAuthenticator().getFamilyName() != null) {
				legalAuthenticatorName.setFamilyName(documentMetadata.getLegalAuthenticator().getFamilyName());	
			}
			if (documentMetadata.getLegalAuthenticator().getGivenName() != null) {
				legalAuthenticatorName.setGivenName(documentMetadata.getLegalAuthenticator().getGivenName());	
			}
			if (documentMetadata.getLegalAuthenticator().getSecondAndFurtherGivenNames() != null) {
				legalAuthenticatorName.setSecondAndFurtherGivenNames(documentMetadata.getLegalAuthenticator().getSecondAndFurtherGivenNames());	
			}
			Person legalAuthenticatorPerson = new Person();
			legalAuthenticatorPerson.setName(legalAuthenticatorName);
			documentEntry.setLegalAuthenticator(legalAuthenticatorPerson);
		}

		// Availability Status (enumeration: APPROVED, SUBMITTED, DEPRECATED)
		documentEntry.setAvailabilityStatus(documentMetadata.getAvailabilityStatus());
		
		if (documentMetadata.getClassCode() != null) {
			documentEntry.setClassCode(createCode(documentMetadata.getClassCode()));
		}
		
		if (documentMetadata.getConfidentialityCode() != null) {
			// Code name is most likely null
			LocalizedString confidentialityName = documentMetadata.getConfidentialityCode().getName()!=null? createLocalizedString(documentMetadata.getConfidentialityCode().getName()):createLocalizedString(documentMetadata.getConfidentialityCode().getCode());
			Code confidentialityCode = new Code(documentMetadata.getConfidentialityCode().getCode(), confidentialityName, documentMetadata.getConfidentialityCode().getCodingScheme());
			documentEntry.getConfidentialityCodes().add(confidentialityCode);
		}

		// Dates 
		if (documentMetadata.getReportTime() != null) {
			documentEntry.setCreationTime(documentMetadata.getReportTimeStringUTC());
		}
		if (documentMetadata.getServiceStartTime() != null) {
			documentEntry.setServiceStartTime(documentMetadata.getServiceStartTimeStringUTC());
		}
		if (documentMetadata.getServiceStopTime() != null) {
			documentEntry.setServiceStopTime(documentMetadata.getServiceStopTimeStringUTC());
		}
		

		List<Code> eventCodesEntry = documentEntry.getEventCodeList();
		if (documentMetadata.getEventCodes() != null) {
			for (dk.medcom.cda.dto.Code eventCode : documentMetadata.getEventCodes()) {
				eventCodesEntry.add(createCode(eventCode));
			}
		}
		if (documentMetadata.getFormatCode() != null) {
			documentEntry.setFormatCode(createCode(documentMetadata.getFormatCode()));
		}
		if (documentMetadata.getHealthcareFacilityTypeCode() != null) {
			documentEntry.setHealthcareFacilityTypeCode(createCode(documentMetadata.getHealthcareFacilityTypeCode()));
		}
		if (documentMetadata.getLanguageCode() != null) {
			documentEntry.setLanguageCode(documentMetadata.getLanguageCode());
		}
		if (documentMetadata.getMimeType() != null) {
			documentEntry.setMimeType(documentMetadata.getMimeType());
		}
		documentEntry.setType(DocumentEntryType.STABLE);
		if (documentMetadata.getTitle() != null) {
			documentEntry.setTitle(createLocalizedString(documentMetadata.getTitle()));
		}
		if (documentMetadata.getTypeCode() != null) {
			Code typeCode = new Code(documentMetadata.getTypeCode().getCode(), createLocalizedString(documentMetadata.getTypeCode().getName()), documentMetadata.getTypeCode().getCodingScheme());
			documentEntry.setTypeCode(typeCode);
		}
		if (documentMetadata.getPracticeSettingCode() != null) {
			documentEntry.setPracticeSettingCode(createCode(documentMetadata.getPracticeSettingCode()));
		}
		String extenalDocumentId = null;
		if (documentMetadata.getUniqueId() != null) {
			extenalDocumentId = documentMetadata.getUniqueId();
		}
		documentEntry.setUniqueId(extenalDocumentId); 

		documentEntry.setLogicalUuid(entryUuid);
		
		if (documentMetadata.getHomeCommunityId() != null) {
			documentEntry.setHomeCommunityId(documentMetadata.getHomeCommunityId());	
		}
		

		Document document = new Document(documentEntry, new DataHandler(new ByteArrayDataSource(documentPayload.getBytes(), documentMetadata.getMimeType())));
		provideAndRegisterDocumentSet.getDocuments().add(document);

		// Create SubmissionSet for the document
		String submissionSetUuid = generateUUID();
		String submissionSetId = generateUUIDasOID();
		SubmissionSet submissionSet = new SubmissionSet();
		submissionSet.setUniqueId(submissionSetId);
		submissionSet.setSourceId(submissionSetId);
		submissionSet.setLogicalUuid(submissionSetUuid);
		submissionSet.setEntryUuid(submissionSetUuid);
		submissionSet.setPatientId(patientIdentifiable);
		submissionSet.setTitle(createLocalizedString(submissionSetUuid));
		submissionSet.setAuthor(author);
		submissionSet.setAvailabilityStatus(documentMetadata.getAvailabilityStatus());
		submissionSet.setHomeCommunityId(documentEntry.getHomeCommunityId());
		

		if (documentMetadata.getReportTime() != null) {
			submissionSet.setSubmissionTime(documentMetadata.getReportTimeStringUTC());
		}
		submissionSet.setContentTypeCode(documentEntry.getTypeCode());
		
		provideAndRegisterDocumentSet.setSubmissionSet(submissionSet);

		// Associate the SubmissionSet with the DocumentEntry
		Association association = new Association();
		association.setAssociationType(AssociationType.HAS_MEMBER);
		association.setEntryUuid(generateUUID());
		association.setSourceUuid(submissionSet.getEntryUuid());
		association.setTargetUuid(documentEntry.getEntryUuid());
		association.setAvailabilityStatus(documentMetadata.getAvailabilityStatus());
		association.setLabel(AssociationLabel.ORIGINAL);
		provideAndRegisterDocumentSet.getAssociations().add(association);
		if (externalIdForDocumentToReplace != null) {
			Association replacementAssociation = new Association(AssociationType.REPLACE, generateUUID(), documentEntry.getEntryUuid(), externalIdForDocumentToReplace);
			provideAndRegisterDocumentSet.getAssociations().add(replacementAssociation);
		}

		// Transform request
		ProvideAndRegisterDocumentSetTransformer registerDocumentSetTransformer = new ProvideAndRegisterDocumentSetTransformer(getEbXmlFactory());
		EbXMLProvideAndRegisterDocumentSetRequest30 ebxmlRequest = (EbXMLProvideAndRegisterDocumentSetRequest30) registerDocumentSetTransformer.toEbXML(provideAndRegisterDocumentSet);
		ProvideAndRegisterDocumentSetRequestType provideAndRegisterDocumentSetRequestType = ebxmlRequest.getInternal();

		return provideAndRegisterDocumentSetRequestType;
	}
	
	private Code createCode(dk.medcom.cda.dto.Code code) {
		Code result = new Code(code.getCode(), createLocalizedString(code.getName()), code.getCodingScheme());
		return result;
	}

	private static final EbXMLFactory ebXMLFactory = new EbXMLFactory30();

	protected EbXMLFactory getEbXmlFactory() {
		return ebXMLFactory;
	}

	private String generateUUID() {
		return java.util.UUID.randomUUID().toString();
	}
	
	private String generateUUIDasOID() {
		java.util.UUID uuid = java.util.UUID.randomUUID();
		return Math.abs(uuid.getLeastSignificantBits()) + "." + Math.abs(uuid.getMostSignificantBits())+"."+Calendar.getInstance().getTimeInMillis();
	}
	
	private LocalizedString createLocalizedString(String string) {
		return new LocalizedString(string, "da-DK", "UTF-8");
	}
	
	
	public SubmitObjectsRequest buildDeprecateSubmitObjectsRequest(String entryUUidToDeprecate, DeprecateInformation deprecateInformation) {

		String submissionSetid = generateUUID();
		SubmissionSet ss = createSubmissionSetForDocumentEntry(submissionSetid, entryUUidToDeprecate, deprecateInformation);
		Association as = createAssociateDeprecateStatus(ss, entryUUidToDeprecate, deprecateInformation);

		RegisterDocumentSet request = new RegisterDocumentSet();
		request.setSubmissionSet(ss);
		request.getAssociations().add(as);
		
		return createSubmitObjectsRequest(request);
	}
	
	private SubmitObjectsRequest createSubmitObjectsRequest(RegisterDocumentSet request) {
		RegisterDocumentSetTransformer requestTransformer = new RegisterDocumentSetTransformer(ebXMLFactory);
		EbXMLSubmitObjectsRequest30 ebxmlRequest = (EbXMLSubmitObjectsRequest30) requestTransformer.toEbXML(request);
		SubmitObjectsRequest submitObjectsRequest = ebxmlRequest.getInternal();
		
		return submitObjectsRequest;
	}
	
	
	private SubmissionSet createSubmissionSetForDocumentEntry(String id, String entryUUidToDeprecate, DeprecateInformation deprecateInformation) {
		SubmissionSet submissionSet = new SubmissionSet();
		submissionSet.setEntryUuid(id);
		submissionSet.setLogicalUuid(id);
		submissionSet.setUniqueId(entryUUidToDeprecate);
		submissionSet.setPatientId(new Identifiable(deprecateInformation.getCpr(), new AssigningAuthority(NSI.CPR_OID)));
		submissionSet.setSourceId(deprecateInformation.getRepositoryuniqueid());
		submissionSet.setContentTypeCode(createCode(deprecateInformation.getTypeCode()));
		submissionSet.setSubmissionTime(new DateTime());
		submissionSet.setHomeCommunityId(deprecateInformation.getHomeCommunityId());
		return submissionSet;
	}
	
	
	private Association createAssociateDeprecateStatus(SubmissionSet submissionSet, String entryUUidToDeprecate, DeprecateInformation deprecateInformation) {
		Association asso = new Association();
		asso.setAssociationType(AssociationType.UPDATE_AVAILABILITY_STATUS);
		asso.setSourceUuid(submissionSet.getLogicalUuid());
		asso.setTargetUuid(entryUUidToDeprecate);
		
		asso.setOriginalStatus(deprecateInformation.getStatus());
		
		asso.setNewStatus(AvailabilityStatus.DEPRECATED);
		
		return asso;
	}

}
