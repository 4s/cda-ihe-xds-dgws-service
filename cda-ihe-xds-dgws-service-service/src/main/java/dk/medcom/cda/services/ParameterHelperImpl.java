package dk.medcom.cda.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.medcom.cda.dto.Code;
import dk.medcom.cda.exceptions.ParameterException;

public class ParameterHelperImpl implements ParameterHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(ParameterHelperImpl.class);
	private PropertiesList propertiesService;
	private FunctionCode functionCode; 
	private String cdaFileName;
	private String entryUuid;
	private String entryUuidToUpdate;
	private String cpr;
	
	private String entryUuidPrefix = "urn:uuid:";
	
	public ParameterHelperImpl(PropertiesList propertiesService, String... args) throws ParameterException {
		this.propertiesService = propertiesService;
//		for (int i =  0; i < args.length; i++ ) {
//			LOGGER.info("Arg " + i + " is: " + args[i]);
//		}
			
		
		if (args.length > 0) {
			try {
				this.functionCode = FunctionCode.valueOf(args[0].toUpperCase());
				LOGGER.info("Function is: " + this.functionCode);				
			} catch (IllegalArgumentException e) {
				throw new ParameterException("The functionCode is not valid: " + args[0] , e);
			}
		} else {
			throw new ParameterException("functionCode must be given");
		}
		
		switch (this.functionCode) {
		case CREATE:
			if (args.length > 1) {
				this.cdaFileName = args[1];
				LOGGER.info("Filename is: " + this.cdaFileName);
			}
			if (args.length > 2) {
				this.entryUuid = args[2];
				LOGGER.info("entryUuid is: " + this.entryUuid);
			}
			break;
		case REPLACE: 
			if (args.length > 1) {
				this.cdaFileName = args[1];
				LOGGER.info("Filename is: " + this.cdaFileName);
			}
			if (args.length > 2) {
				this.entryUuidToUpdate = args[2];
				LOGGER.info("entryUuid to replace is: " + this.getEntryUuidToUpdate());
			}
			if (args.length > 3) {
				this.entryUuid = args[3];
				LOGGER.info("entryUuid is: " + this.entryUuid);
			}
			break;
		case DEPRECATE:
			if (args.length > 1) {
				this.entryUuidToUpdate = args[1];
				LOGGER.info("entryUuid to deprecate is: " + this.getEntryUuidToUpdate());
			}
			if (args.length > 2) {
				this.cpr = args[2];
				LOGGER.info("length of cpr to deprecate is: " + this.getCpr().length());
			}
			break;
		default:
			throw new ParameterException("functionCode not available");
		}
		

	}

	
	public void validate() throws ParameterException {
		
		//parameters always in use
		checkValid("keystore.filename", propertiesService.getKeystoreFilename());
		checkValid("sts.url", propertiesService.getStsUrl());
		checkValid("keystore.alias", propertiesService.getKeystoreAlias());
		checkValid("keystore.password}", propertiesService.getKeystorePassword());
		checkValid("medcom.cvr", propertiesService.getCvr());
		checkValid("medcom.orgname", propertiesService.getOrgname());
		checkValid("medcom.itsystem", propertiesService.getItsystem());

		
		switch (this.functionCode) {
		case CREATE:
			validateCreate();
			break;
		case REPLACE: 
			validateCreate();
			checkValid("entryUuid to replace", this.entryUuidToUpdate);
			break;
		case DEPRECATE:
			validateDeprecate();
			break;
		}
		
		LOGGER.info("Parameters are approved");
	}
	
	private void validateCreate() throws ParameterException {
		
		checkFileExist(this.cdaFileName);

		checkValid("xds.iti41.endpoint", propertiesService.getXdsIti41Endpoint());

		checkValid("classcode.name", propertiesService.getClassCodeName());
		checkValid("classcode.code", propertiesService.getClassCodeCode());
		checkValid("classcode.codingscheme", propertiesService.getClassCodeCodingScheme());
		checkValid("formatcode.name", propertiesService.getFormatCodeName());
		checkValid("formatcode.code}", propertiesService.getFormatCodeCode());
		checkValid("formatcode.codingscheme", propertiesService.getFormatCodeCodingScheme());
		checkValid("healthcarefacilitytypecode.name", propertiesService.getHealthcareFacilityTypeCodeName());
		checkValid("healthcarefacilitytypecode.code", propertiesService.getHealthcareFacilityTypeCodeCode());
		checkValid("healthcarefacilitytypecode.codingscheme", propertiesService.getHealthcareFacilityTypeCodingScheme());
		checkValid("practicesettingcode.name", propertiesService.getPracticeSettingCodeName());
		checkValid("practicesettingcode.code", propertiesService.getPracticeSettingCodeCode());
		checkValid("practicesettingcode.codingscheme", propertiesService.getPracticeSettingCodingScheme());
		checkValid("xds.homecommunityid", propertiesService.getHomeCommunityId());
		
		if (this.entryUuid != null) {
			if (this.entryUuid.isEmpty()) {
				throw new ParameterException("entryUuid must not be empty");
			}
			if (!(this.entryUuid.length() > entryUuidPrefix.length() &&  this.entryUuid.subSequence(0, entryUuidPrefix.length()).equals(entryUuidPrefix))) {
				throw new ParameterException("entryUuid must be prefixed with: " + entryUuidPrefix); 
			}
		} 
		
	}
	
	private void validateDeprecate() throws ParameterException {

		checkValid("xds.iti57.endpoint", propertiesService.getXdsIti57Endpoint());

		checkValid("entryUuid to deprecate", this.entryUuidToUpdate);
		checkValid("cpr", this.cpr);
		
		checkValid("xds.repositoryuniqueid", propertiesService.getRepositoryuniqueid());
		
		checkValid("typecode.name", propertiesService.getTypeCodeName());
		checkValid("typecode.code}", propertiesService.getTypeCodeCode());
		checkValid("typecode.codingscheme", propertiesService.getTypeCodeCodingScheme());
		
		checkValid("xds.homecommunityid", propertiesService.getHomeCommunityId());
		
		
	}
	
	private void checkFileExist(String fileName) throws ParameterException {
		try {
			checkValid("CDA filename", fileName);
			Files.readAllBytes(Paths.get(fileName));
		} catch (IOException e) {
			throw new ParameterException("CDA file could not be found at: " + fileName , e);
		}
	}
	
	private void checkValid(String parameterName, String value) throws ParameterException {
		if (value == null || value.isEmpty()) {
			throw new ParameterException(parameterName + " is not available");	
		}
	}
	
	
	
	@Override
	public Code getClassCode() {
		Code code = new Code(propertiesService.getClassCodeName(), propertiesService.getClassCodeCode(), propertiesService.getClassCodeCodingScheme());
		LOGGER.debug("Getting classCode with values: " + code.toString());
		return code;
	}
	@Override
	public Code getFormatCode() {
		Code code = new Code(propertiesService.getFormatCodeName(), propertiesService.getFormatCodeCode(), propertiesService.getFormatCodeCodingScheme());
		LOGGER.debug("Getting formatCode with values: " + code.toString());
		return code;
	}
	@Override
	public Code getHealthcareFacilityTypeCode() {
		Code code = new Code(propertiesService.getHealthcareFacilityTypeCodeName(), propertiesService.getHealthcareFacilityTypeCodeCode(), propertiesService.getHealthcareFacilityTypeCodingScheme());
		LOGGER.debug("Getting healthcareFacilityTypeCode with values: " + code.toString());
		return code;
	}
	@Override
	public Code getPracticeSettingCode() {
		Code code = new Code(propertiesService.getPracticeSettingCodeName(), propertiesService.getPracticeSettingCodeCode(),propertiesService.getPracticeSettingCodingScheme());
		LOGGER.debug("Getting practiceSettingCode with values: " + code.toString());
		return code;
	}
	
	@Override
	public String getCdaFileName() {
		return cdaFileName;
	}
	
	@Override
	public String getEntryUuid() {
		
		if (this.entryUuid == null) {
			this.entryUuid = entryUuidPrefix + java.util.UUID.randomUUID().toString();
		}
		
		return this.entryUuid;
		
	}
	
	@Override
	public String getEntryUuidToUpdate() {
		return entryUuidToUpdate;
	}

	@Override
	public FunctionCode getFunctionCode() {
		return functionCode;
	}
	
	@Override
	public String getCpr() {
		return cpr;
	}


	@Override
	public Code getTypeCode() {
		Code code = new Code(propertiesService.getTypeCodeName(), propertiesService.getTypeCodeCode(),propertiesService.getTypeCodeCodingScheme());
		LOGGER.debug("Getting typecode with values: " + code.toString());
		return code;
	}


	@Override
	public String getRepositoryuniqueid() {
		return propertiesService.getRepositoryuniqueid(); 
	}

	@Override
	public String getHomeCommunityId() {
		return propertiesService.getHomeCommunityId(); 
	}


}
