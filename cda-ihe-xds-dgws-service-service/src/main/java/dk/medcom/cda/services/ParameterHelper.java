package dk.medcom.cda.services;

import dk.medcom.cda.dto.Code;

public interface ParameterHelper {
	
	enum FunctionCode {
		  CREATE,
		  REPLACE,
		  DEPRECATE
		}

	public Code getClassCode();
	public Code getFormatCode();
	public Code getHealthcareFacilityTypeCode();
	public Code getPracticeSettingCode();
	public Code getTypeCode();
	public String getCdaFileName();
	public String getEntryUuid();
	public String getEntryUuidToUpdate();
	public String getCpr();
	public String getRepositoryuniqueid();
	public FunctionCode getFunctionCode();
	public String getHomeCommunityId();

}
