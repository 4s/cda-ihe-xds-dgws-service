package dk.medcom.cda.services;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import dk.medcom.cda.document.DocumentHelper;
import dk.medcom.cda.dto.DocumentMetadata;
import dk.medcom.cda.exceptions.XdsException;
import dk.medcom.cda.services.ParameterHelper.FunctionCode;

public class DocumentProcessor {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentProcessor.class);
	
	@Autowired
	private XdsRequestService xdsRequestService;
	
	@Autowired
	private CdaMetaDataFactory cdaMetaDataFactory;
	
	public DocumentProcessor() {
		
	}
	//For test purpose
	public DocumentProcessor(XdsRequestService xdsRequestService, CdaMetaDataFactory cdaMetaDataFactory) {
		this.xdsRequestService = xdsRequestService;
		this.cdaMetaDataFactory = cdaMetaDataFactory;
	}
	
	public void runCDADocument(FunctionCode functionCode, DocumentHelper documentHelper) throws IOException, XdsException {
		
		
		switch (functionCode) {
		case CREATE:
			doCreate(documentHelper);
			break;
		case REPLACE: 
			doReplace(documentHelper);	
			break;
		case DEPRECATE:
			doDeprecate(documentHelper);
			break;
		}
		
		

	}
	
	private void doCreate(DocumentHelper documentHelper) throws IOException, XdsException {
		
		// Get the XML document
		String xmlDocument = documentHelper.createDocumentAsXML();

		// Create metadata using builder/parser, default values and parameters values
		DocumentMetadata documentMetadata = cdaMetaDataFactory.getMetadata(documentHelper.createCdaMetadata(), xmlDocument);
		
		// Register the document
		String documentId = xdsRequestService.createAndRegisterDocument(xmlDocument , documentMetadata, documentHelper.getEntryUuid(), null);

		// Log successfull call
		LOGGER.info("A new document is registered with DocumentId = "+documentId + " and entryUUid = " + documentHelper.getEntryUuid());
		
	}
	
	private void doReplace(DocumentHelper documentHelper) throws IOException, XdsException {
		
		// Get the XML document
		String xmlDocument = documentHelper.createDocumentAsXML();

		// Create metadata using builder/parser, default values and parameters values
		DocumentMetadata documentMetadata = cdaMetaDataFactory.getMetadata(documentHelper.createCdaMetadata(), xmlDocument);
		
		// Update the document
		String documentId = xdsRequestService.createAndRegisterDocument(xmlDocument , documentMetadata, documentHelper.getEntryUuid(), documentHelper.getEntryUuidToUpdate());

		// Log successfull call
		LOGGER.info("A document is registered with DocumentId = "+documentId + " and entryUUid = " + documentHelper.getEntryUuid() + " as a replacement of document with entryUUid = " + documentHelper.getEntryUuidToUpdate());
		
	}
	
	private void doDeprecate(DocumentHelper documentHelper) throws IOException, XdsException {
		
		// Deprecate the document
		xdsRequestService.deprecateDocument(documentHelper.getEntryUuidToUpdate(), documentHelper.getDeprecateInformation());

		// Log successfull call
		LOGGER.info("A document is deprecated with entryUUid = " + documentHelper.getEntryUuidToUpdate() );
		
	}


}
