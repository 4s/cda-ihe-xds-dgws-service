package dk.medcom.cda.services;

import org.springframework.beans.factory.annotation.Value;

public class PropertiesList {
	
	@Value("${xds.iti41.endpoint}")
	private String xdsIti41Endpoint;

	@Value("${xds.iti57.endpoint}")
	private String xdsIti57Endpoint;
	
	@Value("${sts.url}")
	private String stsUrl;	
	
	@Value("${keystore.alias}")
	private String keystoreAlias;
	@Value("${keystore.filename}")
	private String keystoreFilename;
	@Value("${keystore.password}")
	private String keystorePassword;
	
	@Value("${xds.repositoryuniqueid}")
	private String repositoryuniqueid;	


	@Value("${medcom.cvr}")
	private String cvr;
	@Value("${medcom.orgname}")
	private String orgname;
	@Value("${medcom.itsystem}")
	private String itsystem;
	
	@Value("${classcode.name}")
	private String classCodeName;
	@Value("${classcode.code}")
	private String classCodeCode;
	@Value("${classcode.codingscheme}")
	private String classCodeCodingScheme;
	
	@Value("${formatcode.name}")
	private String formatCodeName;
	@Value("${formatcode.code}")
	private String formatCodeCode;
	@Value("${formatcode.codingscheme}")
	private String formatCodeCodingScheme;
	
	@Value("${healthcarefacilitytypecode.name}")
	private String healthcareFacilityTypeCodeName;
	@Value("${healthcarefacilitytypecode.code}")
	private String healthcareFacilityTypeCodeCode;
	@Value("${healthcarefacilitytypecode.codingscheme}")
	private String healthcareFacilityTypeCodingScheme;
	
	@Value("${practicesettingcode.name}")
	private String practiceSettingCodeName;
	@Value("${practicesettingcode.code}")
	private String practiceSettingCodeCode;
	@Value("${practicesettingcode.codingscheme}")
	private String practiceSettingCodingScheme;
	
	@Value("${typecode.name}")
	private String typeCodeName;
	@Value("${typecode.code}")
	private String typeCodeCode;
	@Value("${typecode.codingscheme}")
	private String typeCodeCodingScheme;
	
	@Value("${xds.homecommunityid:#{null}}")
	private String homeCommunityId;
	
	
	public String getXdsIti41Endpoint() {
		return xdsIti41Endpoint;
	}
	public String getXdsIti57Endpoint() {
		return xdsIti57Endpoint;
	}
	public String getStsUrl() {
		return stsUrl;
	}
	public String getKeystoreAlias() {
		return keystoreAlias;
	}
	public String getKeystoreFilename() {
		return keystoreFilename;
	}
	public String getKeystorePassword() {
		return keystorePassword;
	}
	public String getCvr() {
		return cvr;
	}
	public String getOrgname() {
		return orgname;
	}
	public String getItsystem() {
		return itsystem;
	}
	public String getClassCodeName() {
		return classCodeName;
	}
	public String getClassCodeCode() {
		return classCodeCode;
	}
	public String getClassCodeCodingScheme() {
		return classCodeCodingScheme;
	}
	public String getFormatCodeName() {
		return formatCodeName;
	}
	public String getFormatCodeCode() {
		return formatCodeCode;
	}
	public String getFormatCodeCodingScheme() {
		return formatCodeCodingScheme;
	}
	public String getHealthcareFacilityTypeCodeName() {
		return healthcareFacilityTypeCodeName;
	}
	public String getHealthcareFacilityTypeCodeCode() {
		return healthcareFacilityTypeCodeCode;
	}
	public String getHealthcareFacilityTypeCodingScheme() {
		return healthcareFacilityTypeCodingScheme;
	}
	public String getPracticeSettingCodeName() {
		return practiceSettingCodeName;
	}
	public String getPracticeSettingCodeCode() {
		return practiceSettingCodeCode;
	}
	public String getPracticeSettingCodingScheme() {
		return practiceSettingCodingScheme;
	}
	public String getRepositoryuniqueid() {
		return repositoryuniqueid;
	}
	public String getTypeCodeName() {
		return typeCodeName;
	}
	public String getTypeCodeCode() {
		return typeCodeCode;
	}
	public String getTypeCodeCodingScheme() {
		return typeCodeCodingScheme;
	}
	public String getHomeCommunityId() {
		return homeCommunityId;
	}
	
}
