package dk.medcom.cda.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.medcom.cda.dto.Code;
import dk.s4.hl7.cda.convert.CDAMetadataXmlCodec;
import dk.s4.hl7.cda.convert.QRDXmlCodec;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.cdametadata.CDAMetadata;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;
import dk.s4.hl7.cda.model.qrd.QRDResponse;

public class CdaDocumentService {
	private static Logger LOGGER = LoggerFactory.getLogger(CdaDocumentService.class);
	private static String QRD = "74465-6";
	private static String QFDD = "74468-0";
	private static String PHMR = "53576-5";
	private static String APD = "39289-4";
	
	public CDAMetadata decodeMetadata(String documentContent) {
		CDAMetadataXmlCodec codec = new CDAMetadataXmlCodec();
		return codec.decode(documentContent);
	}
	
	public Code getProjectRef(CDAMetadata cdaMetadataDecoded, String document) {
		String typeCode = getTypeCode(cdaMetadataDecoded);
		if (typeCode != null) {
			ClinicalDocument clinicalDocument = decodeClinicalDocument(document, cdaMetadataDecoded, typeCode);
			if (clinicalDocument != null) {
				Code eventCode = getProjectReference(typeCode, clinicalDocument);
				return eventCode;
			}
		}
		return null;
	}
	
	private ClinicalDocument decodeClinicalDocument(String documentContent, CDAMetadata cDAMetadata, String typeCode) {
		
		if (isQrd(typeCode)) {
			QRDXmlCodec codecQRD = new QRDXmlCodec();
			return codecQRD.decode(documentContent);
		}
		
		return null;
	}
	
	  private String getTypeCode(CDAMetadata cdaMetadata) {
		  String value = (cdaMetadata.getCodeCodedValue() == null) ? null : cdaMetadata.getCodeCodedValue().getCode();
		  LOGGER.info("typeCode found is:" + value);
		  return value;
	  }

	  private Code getProjectReference(String typeCode, ClinicalDocument clinicalDocument) {
		  if (isQrd(typeCode) && clinicalDocument instanceof QRDDocument) {
			  QRDDocument qrdDocument = (QRDDocument) clinicalDocument;
			  return loopForQrdReference(qrdDocument);
		  }
		  return null;
	  }

	  private boolean isQrd(String typeCode) {
		  return QRD.equals(typeCode);
	  }
	  
	  private Code loopForQrdReference(QRDDocument qrdDocument) {
		  //look through the responses and find the first reference matching
		  for (Section<QRDOrganizer> section : qrdDocument.getSections()) {
			  for (QRDOrganizer qrdOrganizer : section.getOrganizers()) {
				  for (QRDResponse qrdResponse : qrdOrganizer.getQRDResponses()) {
					  for (Reference reference : qrdResponse.getReferences()) {
						  Code ref = getProjectRefAsCode(reference);
						  if (ref != null) {
							  LOGGER.info("reference found is:" + ref.getCode());
							  return ref;
						  }
					  }
				  }
			  }
		  }
		  return null;
	  }

	  private Code getProjectRefAsCode(Reference reference) {
		  if (reference.getExternalObservation() != null) {
			  return null;
		  }
		  if (reference.getReferencesUse() != null) {
			  return null;
		  }
		  if (reference.getExternalDocument() != null && reference.getExternalDocumentType() != null) {
			  String name =  reference.getExternalDocumentType().getDisplayName();
			  String code = reference.getExternalDocument().getExtension();
			  String codingScheme = reference.getExternalDocumentType().getCodeSystem();	
			  Code eventCode = new Code(name,code, codingScheme);
			  return eventCode;
		  }
		  return null;
	  }

}
