package dk.medcom.cda.services;

import javax.xml.ws.WebServiceException;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.EbXMLFactory;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLFactory30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.ProvideAndRegisterDocumentSetRequestType;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.lcm.SubmitObjectsRequest;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rs.RegistryError;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rs.RegistryResponseType;
import org.openehealth.ipf.commons.ihe.xds.iti41.Iti41PortType;
import org.openehealth.ipf.commons.ihe.xds.iti57.Iti57PortType;
import org.springframework.beans.factory.annotation.Autowired;

import dk.medcom.cda.document.DeprecateInformation;
import dk.medcom.cda.dto.DocumentMetadata;
import dk.medcom.cda.exceptions.XdsException;

public class XdsRequestService {

	private static final EbXMLFactory ebXMLFactory = new EbXMLFactory30();

	@Autowired
	XdsRequestBuilderService xdsRequestBuilderService;

	@Autowired
	Iti41PortType iti41PortType;

	@Autowired
	Iti57PortType iti57PortType;
	
	
	public String createAndRegisterDocument(String document, DocumentMetadata documentMetadata, String entryUuid, String externalIdForDocumentToReplace) throws XdsException {
		try {
			ProvideAndRegisterDocumentSetRequestType provideAndRegisterDocumentSetRequest = xdsRequestBuilderService.buildProvideAndRegisterDocumentSetRequest(document, documentMetadata, entryUuid, externalIdForDocumentToReplace);
			RegistryResponseType registryResponse = iti41PortType.documentRepositoryProvideAndRegisterDocumentSetB(provideAndRegisterDocumentSetRequest);
			if (registryResponse.getRegistryErrorList() == null || registryResponse.getRegistryErrorList().getRegistryError() == null || registryResponse.getRegistryErrorList().getRegistryError().isEmpty()) {
				return documentMetadata.getUniqueId();
			} else {
				XdsException e = new XdsException("The XDS reported errors during iti41 call");
				for (RegistryError registryError :registryResponse.getRegistryErrorList().getRegistryError()) {
					e.addError(registryError.getCodeContext());
				}
				throw e;
			}
		} catch (WebServiceException e) {
			throw new XdsException("Error calling externally" , e);
		}

	}
	
	public void deprecateDocument(String entryUUidToDeprecate, DeprecateInformation deprecateInformation) throws XdsException {
		SubmitObjectsRequest body = xdsRequestBuilderService.buildDeprecateSubmitObjectsRequest(entryUUidToDeprecate, deprecateInformation);		
		RegistryResponseType registryResponse = iti57PortType.documentRegistryUpdateDocumentSet(body);

		if (registryResponse.getRegistryErrorList() == null || registryResponse.getRegistryErrorList().getRegistryError() == null || registryResponse.getRegistryErrorList().getRegistryError().isEmpty()) {
			//OK !
		} else {
			XdsException e = new XdsException("The XDS reported errors during iti57 call");
			for (RegistryError registryError :registryResponse.getRegistryErrorList().getRegistryError()) {
				e.addError(registryError.getCodeContext());
			}
			throw e;
		}
	}

	protected EbXMLFactory getEbXmlFactory() {
		return ebXMLFactory;
	}

	public void addOutInterceptors(AbstractPhaseInterceptor<Message> interceptor) {		
		addOutInterceptor(iti41PortType, interceptor);		
	}

	private void addOutInterceptor(Object o, AbstractPhaseInterceptor<Message> interceptor) {
		Client proxy = ClientProxy.getClient(o);
		proxy.getOutInterceptors().add(interceptor);
	}

	public void addInInterceptors(AbstractPhaseInterceptor<Message> interceptor) {		
		addInInterceptor(iti41PortType, interceptor);		
	}

	private void addInInterceptor(Object o, AbstractPhaseInterceptor<Message> interceptor) {
		Client proxy = ClientProxy.getClient(o);
		proxy.getInInterceptors().add(interceptor);
	}

	
}
