package dk.medcom.cda.exceptions;

public class ParameterException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ParameterException(String msg) {
		super(msg);
	}
	
	public ParameterException(String msg, Exception e) {
		super(msg, e);
	}

}
