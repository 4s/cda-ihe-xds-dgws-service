package dk.medcom.cda.configuration;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import dk.sosi.seal.vault.CredentialVault;
import dk.sosi.seal.vault.CredentialVaultException;
import dk.sosi.seal.vault.FileBasedCredentialVault;

public class CredentialVaultConfiguration {
	
	@Value("${keystore.filename}")
	private String keystoreFilename;

	@Value("${keystore.password}")
	private String keystorePassword;
	
	@Value("${keystore.alias}")
	private String keystoreAlias;

	@Bean
	public CredentialVault getVault() throws CredentialVaultException, IOException {
		System.setProperty("dk.sosi.seal.vault.CredentialVault#Alias", keystoreAlias);
		File keystoreFile = new File(keystoreFilename);
		return new FileBasedCredentialVault(new Properties(), keystoreFile, keystorePassword);
	}

}
