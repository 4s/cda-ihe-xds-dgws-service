package dk.medcom.cda.configuration;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.openehealth.ipf.commons.ihe.xds.iti41.Iti41PortType;
import org.openehealth.ipf.commons.ihe.xds.iti57.Iti57PortType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import dk.medcom.cda.dgws.DgwsSoapDecorator;
import dk.medcom.cda.dgws.DisableMustUnderstandInterceptor;
import dk.medcom.cda.dgws.STSRequestHelper;
import dk.medcom.dgws.DgwsContext;
import dk.sosi.seal.SOSIFactory;
import dk.sosi.seal.pki.SOSIFederation;
import dk.sosi.seal.pki.SOSITestFederation;
import dk.sosi.seal.vault.CredentialVault;

public class DgwsConfiguration {

	@Value("${keystore.alias}")
	private String keystoreAlias;

	@Value("${nps.test}")
	private boolean nspIsTest;

	@Autowired
	private ApplicationContext appContext;
	
	@Autowired
	private CredentialVault credentialVault;

	@PostConstruct 
	public void init() {
		// The SOSI Seal Library reads the alias value using System.getProperty()
		System.setProperty("dk.sosi.seal.vault.CredentialVault#Alias", keystoreAlias);
		
		DgwsSoapDecorator dgwsSoapDecorator = appContext.getBean("dgwsSoapDecorator", DgwsSoapDecorator.class);
		DisableMustUnderstandInterceptor dmui = appContext.getBean(DisableMustUnderstandInterceptor.class);

		// Add DGWS decorator to all ITI beans
		Iti41PortType iti41 = appContext.getBean(Iti41PortType.class);
		Client proxy41 = ClientProxy.getClient(iti41);
		proxy41.getOutInterceptors().add(dgwsSoapDecorator);
		proxy41.getOutInterceptors().add(dmui);
		
		Iti57PortType iti57 = appContext.getBean(Iti57PortType.class);
		Client proxy57 = ClientProxy.getClient(iti57);
		proxy57.getOutInterceptors().add(dgwsSoapDecorator);
		proxy57.getOutInterceptors().add(dmui);
		
	}
	@Bean
	public DgwsContext dgwsContext() {
		return new DgwsContext();
	}
	
	@Bean
	public DgwsSoapDecorator dgwsSoapDecorator() {
		return new DgwsSoapDecorator();
	}

	@Bean
	public DisableMustUnderstandInterceptor mustUnderstandInterceptor() {
		return new DisableMustUnderstandInterceptor();
	}
	
	@Bean
	public SOSIFactory createSOSIFactory() throws CertificateException, IOException {
		Properties props = new Properties(System.getProperties());
		props.setProperty(SOSIFactory.PROPERTYNAME_SOSI_VALIDATE, Boolean.toString(true));
		
		if (nspIsTest) {
			return new SOSIFactory(new SOSITestFederation(new Properties()), credentialVault, props);
		} else  {
			return new SOSIFactory(new SOSIFederation(new Properties()), credentialVault, props);
		}

	}

	@Bean
	public STSRequestHelper stsRequestHelper() {
		STSRequestHelper requestHelper = new STSRequestHelper();
		return requestHelper;
	}

	@Bean(name="dgwsTemplate")
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
