package dk.medcom.cda.configuration;

import javax.xml.namespace.QName;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.transport.http.HTTPConduit;
import org.openehealth.ipf.commons.ihe.ws.WsTransactionConfiguration;
import org.openehealth.ipf.commons.ihe.xds.core.XdsClientFactory;
import org.openehealth.ipf.commons.ihe.xds.iti41.Iti41PortType;
import org.openehealth.ipf.commons.ihe.xds.iti57.Iti57PortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import dk.medcom.cda.services.CdaDocumentService;
import dk.medcom.cda.services.CdaMetaDataFactory;
import dk.medcom.cda.services.DocumentProcessor;
import dk.medcom.cda.services.PropertiesList;
import dk.medcom.cda.services.XdsRequestBuilderService;
import dk.medcom.cda.services.XdsRequestService;

public class ApplicationConfiguration {

	private static Logger LOGGER = LoggerFactory.getLogger(ApplicationConfiguration.class);

	@Value("${xds.iti41.endpoint}")
	String xdsIti41Endpoint;
	
	@Value("${xds.iti57.endpoint}")
	String xdsIti57Endpoint;

	@Bean
	public Iti41PortType getDocumentRepositoryServiceIti41() {
		LOGGER.info("Creating Iti41PortType for url: "+xdsIti41Endpoint);
		String xdsIti41Wsdl = "wsdl/iti41.wsdl";	

		XdsClientFactory xdsClientFactory = generateXdsRepositoryClientFactory(xdsIti41Wsdl, xdsIti41Endpoint, Iti41PortType.class);
		Iti41PortType client = (Iti41PortType) xdsClientFactory.getClient();

		initProxy(client);	

		return client;
	}
	
	@Bean
	public Iti57PortType getDocumentRepositoryServiceIti57() {
		LOGGER.info("Creating Iti57PortType for url: "+xdsIti57Endpoint);
		String xdsIti57Wsdl = "wsdl/iti57.wsdl";	

		XdsClientFactory xdsClientFactory = generateXdsRegistryClientFactory("urn:ihe:iti:xds-b:2010", xdsIti57Wsdl, xdsIti57Endpoint, Iti57PortType.class);
		Iti57PortType client = (Iti57PortType) xdsClientFactory.getClient();

		initProxy(client);	

		return client;
	}
	
	private XdsClientFactory generateXdsRegistryClientFactory(String namespace, String wsdl, String url, Class<?> clazz){
		final WsTransactionConfiguration WS_CONFIG = new WsTransactionConfiguration(
				new QName(namespace, "DocumentRegistry_Service",
						"ihe"), clazz, new QName(
								namespace,
								"DocumentRegistry_Binding_Soap12", "ihe"), false,
				wsdl, true, false, false, false);

		return new XdsClientFactory(WS_CONFIG, url, null, null,null);
	}
	
	private XdsClientFactory generateXdsRepositoryClientFactory(String wsdl, String url, Class<?> clazz){
		final WsTransactionConfiguration WS_CONFIG = new WsTransactionConfiguration(
				new QName("urn:ihe:iti:xds-b:2007", "DocumentRepository_Service",
						"ihe"), clazz, new QName(
								"urn:ihe:iti:xds-b:2007",
								"DocumentRepository_Binding_Soap12", "ihe"), true,
				wsdl, true, false, false, false);
		

		XdsClientFactory xcf = new XdsClientFactory(WS_CONFIG, url, null, null,null);
		return xcf;
	}
	
	private void initProxy(Object o) {
		
		Client proxy = ClientProxy.getClient(o);		
		if (LOGGER.isDebugEnabled()) {
			proxy.getOutInterceptors().add(new LoggingOutInterceptor());
			proxy.getInInterceptors().add(new LoggingInInterceptor());
		}	
		HTTPConduit conduit = (HTTPConduit)proxy.getConduit();
		TLSClientParameters tcp = new TLSClientParameters();
		conduit.setTlsClientParameters(tcp);
	}

	@Bean
	public XdsRequestBuilderService xdsRequestBuilderService() {
		XdsRequestBuilderService xdsRequestBuilderService = new XdsRequestBuilderService();
		return xdsRequestBuilderService;
	}

	@Bean
	public XdsRequestService xdsRequestService() {
		XdsRequestService xdsRequestService = new XdsRequestService();
		return xdsRequestService;
	}
	
	@Bean
	public CdaDocumentService cdaDocumentService() {
		return new CdaDocumentService();
	}
	
	@Bean
	public CdaMetaDataFactory cdaMetaDataFactory() {
		return new CdaMetaDataFactory(cdaDocumentService());
	}
	
	@Bean
	public DocumentProcessor documentProcessor() {
		return new DocumentProcessor();
	}
	
	@Bean
	public PropertiesList propertiesList() {
		return new PropertiesList();
	}
}
