package dk.medcom.cda.document;

import org.openehealth.ipf.commons.ihe.xds.core.metadata.AvailabilityStatus;

import dk.medcom.cda.dto.Code;

public class DeprecateInformation {
	
	private String cpr;
	private Code typeCode;
	private String repositoryuniqueid;
	private AvailabilityStatus status;
	private String homeCommunityId;
	
	public DeprecateInformation(String cpr, Code typeCode, String repositoryuniqueid, AvailabilityStatus status, String homeCommunityId) {
		this.cpr = cpr;
		this.typeCode = typeCode;
		this.repositoryuniqueid = repositoryuniqueid;
		this.status = status;
		this.homeCommunityId = homeCommunityId;
	}
	
	public String getCpr() {
		return cpr;
	}
	public Code getTypeCode() {
		return typeCode;
	}
	public String getRepositoryuniqueid() {
		return repositoryuniqueid;
	}
	public AvailabilityStatus getStatus() {
		return status;
	}
	public String getHomeCommunityId() {
		return homeCommunityId;
	}
	

}
