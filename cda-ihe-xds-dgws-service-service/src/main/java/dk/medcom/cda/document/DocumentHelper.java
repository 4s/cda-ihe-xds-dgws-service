package dk.medcom.cda.document;

import java.io.IOException;

import dk.medcom.cda.dto.CdaMetadata;

public interface DocumentHelper {
	
	public String createDocumentAsXML() throws IOException;
	public CdaMetadata createCdaMetadata();
	public String getEntryUuid();
	public String getEntryUuidToUpdate();
	public DeprecateInformation getDeprecateInformation();
}
