package dk.medcom.cda.document;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import org.openehealth.ipf.commons.ihe.xds.core.metadata.AvailabilityStatus;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntryType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.medcom.cda.dto.CdaMetadata;
import dk.medcom.cda.services.ParameterHelper;

public class DocumentHelperFromFileImpl implements DocumentHelper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentHelperFromFileImpl.class);

	private ParameterHelper parameterHelper;
	
	public DocumentHelperFromFileImpl(ParameterHelper parameterHelper) {
		this.parameterHelper = parameterHelper;
	}
	
	@Override
	public String createDocumentAsXML() throws IOException {
		String fileContent = new String(Files.readAllBytes(Paths.get(parameterHelper.getCdaFileName())),StandardCharsets.UTF_8);
		return fileContent;
	}
	@Override
	public CdaMetadata createCdaMetadata() {
		CdaMetadata cdaMetaData = new CdaMetadata();
		cdaMetaData.setAvailabilityStatus(AvailabilityStatus.APPROVED);
		cdaMetaData.setObjectType(DocumentEntryType.STABLE);
		cdaMetaData.setClassCode(parameterHelper.getClassCode());
		cdaMetaData.setFormatCode(parameterHelper.getFormatCode());
		cdaMetaData.setHealthcareFacilityTypeCode(parameterHelper.getHealthcareFacilityTypeCode());
		cdaMetaData.setPracticeSettingCode(parameterHelper.getPracticeSettingCode());
		cdaMetaData.setSubmissionTime(new Date());
		cdaMetaData.setHomeCommunityId(parameterHelper.getHomeCommunityId());
		
		LOGGER.debug("Creating CdaMetadata with values: " + cdaMetaData.toString());
		return cdaMetaData;
	}
	
	@Override
	public String getEntryUuid() {
		return this.parameterHelper.getEntryUuid();
	}
	
	@Override
	public String getEntryUuidToUpdate() {
		return this.parameterHelper.getEntryUuidToUpdate();
	}

	@Override
	public DeprecateInformation getDeprecateInformation() {
		return new DeprecateInformation(parameterHelper.getCpr(), parameterHelper.getTypeCode(), parameterHelper.getRepositoryuniqueid(), AvailabilityStatus.APPROVED, parameterHelper.getHomeCommunityId());
	}
	
}
 