package dk.medcom.cda.dto;

public class Code {

	private String name;
	
	private String code;
	
	private String codingScheme;

	public Code(){}
	
	public Code(String name, String code, String codingScheme){
		this.name = name;
		this.code = code;
		this.codingScheme = codingScheme;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCodingScheme() {
		return codingScheme;
	}

	public void setCodingScheme(String codingScheme) {
		this.codingScheme = codingScheme;
	}

	@Override
	public String toString() {
		return "Code [name=" + name + ", code=" + code + ", codingScheme=" + codingScheme + "]";
	}

	
	
}
