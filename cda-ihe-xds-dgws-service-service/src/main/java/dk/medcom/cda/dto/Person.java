package dk.medcom.cda.dto;

public class Person {
	
	private String givenName;
	private String familyName;
	private String secondAndFurtherGivenNames;
	
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getSecondAndFurtherGivenNames() {
		return secondAndFurtherGivenNames;
	}
	public void setSecondAndFurtherGivenNames(String secondAndFurtherGivenNames) {
		this.secondAndFurtherGivenNames = secondAndFurtherGivenNames;
	}

}
