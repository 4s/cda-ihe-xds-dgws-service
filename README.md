# CDA IHE/XDS DGWS Service

## Overblik
Projektet bygger en jar fil, som kan håndtere følgende funktioner:

* modtage et XML CDA dokument og registrere det (CREATE)
* erstatte et CDA dokument med et nyt (REPLACE)
* slette-markere et CDA dokument med et nyt (DEPRECATE)

Funktionerne kan køre mod forskellige repositories, men er oprindelig udviklet til at gøre adgangen til KIH reposities lettere. I forbindelse med registrering/erstatning udtrækkes metadata fra dokumentet og anvendes.


## Properties
Der arbejdes med følgende properties


| Property                                | Krævet* | Beskrivelse                                                             | 
|-----------------------------------------|---------|-------------------------------------------------------------------------| 
| xds.iti41.endpoint                      | C+R     | Endpoint til XDS servicen iti 41 provide and register                   |
| xds.iti57.endpoint                      | D       | Endpoint til XDS servicen iti 57 deprecate                              |
| xds.repositoryuniqueid                  | D       | Repository uniqueid. Værdi afhænger af de endpoints man benytter        |
| xds.homecommunityid                     | Alle    | HomeCommunityId. Værdi afhænger af de endpoints man benytter            |
|                                         |         |                                                                         |
| sts.url                                 | Alle    | endpoint til den STS, som skal anvendes                                           |
| nsp.test                                | Alle    | Anvendes ved kald til STS. Skal sættes til true for test og false for produktion  |
|                                         |         |                                                                         |
| keystore.filename                       | Alle    | Sti og navn på det keystore, som skal anvendes. Skal være oces3         |
| keystore.alias                          | Alle    | Alias på det certifikat, som skal anvendes                              |
| keystore.password                       | Alle    | password til det keystore som skal anvendes                             |
|                                         |         |                                                                         |
| Eksempel fra keystore indhold:          |         | Owner: SERIALNUMBER=CVR:30808460-FID:94731315 + CN=TU GENEREL FOCES gyldig (funktionscertifikat), O=NETS DANID A/S // CVR:30808460, C=DK |
| medcom.cvr                              | Alle    | Skal matche cvr nummer i keystore. I eksempel er det "30808460"                          |
| medcom.orgname                          | Alle    | Skal matche keystore. I eksempel er det "TU GENEREL FOCES gyldig (funktionscertifikat)"  |
| medcom.itsystem                         | Alle    | Angiv it system                                                         |
|                                         |         |                                                                         |
| Medcoms koder:                          |         | Skal overholde Medcoms standard værdier                                 |
| classcode.name                          | C+R     | Eksempel: Clinical report                                               |
| classcode.code                          | C+R     | Eksempel: 001                                                           |
| classcode.codingscheme                  | C+R     | Eksempel: 1.2.208.184.100.9                                             |
| formatcode.name                         | C+R     | Eksempel: DK PHMR schema                                                |
| formatcode.code                         | C+R     | Eksempel: urn:ad:dk:medcom:phmr:full                                    |
| formatcode.codingscheme                 | C+R     | Eksempel: 1.2.208.184.100.10                                            |
| healthcarefacilitytypecode.name         | C+R     | Eksempel: hjemmesygepleje                                               |
| healthcarefacilitytypecode.code         | C+R     | Eksempel: 550621000005101                                               |
| healthcarefacilitytypecode.codingscheme | C+R     | Eksempel: 2.16.840.1.113883.6.96                                        |
| practicesettingcode.name                | C+R     | Eksempel: almen medicin                                                 |
| practicesettingcode.code                | C+R     | Eksempel: 408443003                                                     |
| practicesettingcode.codingscheme        | C+R     | Eksempel: 2.16.840.1.113883.6.96                                        |
| typecode.name                           | D       | Eksempel: Personal Health Monitoring Report                             |
| typecode.code                           | D       | Eksempel: 53576-5                                                       |
| typecode.codingscheme                   | D       | Eksempel: 2.16.840.1.113883.6.1                                         |
|                                         |         |                                                                         |
| logging.level.dk.medcom                 |         | Når sat som: logging.level.dk.medcom:DEBUG, vil der logges mere. Herunder selve request og response |

(* Krævet: afhænger af funktionskode (C)reate, (R)eplace, (D)eprecate og Alle. Parameteren skal altid være der. Men krævet betyder om den SKAL have en værdi eller ej)


## Info til anvendere
cda-ihe-xds-dgws-service-as-file-1.0.0.jar (version varierer) kaldes med følgende parametre:

* en funktionskode (CREATE,  REPLACE eller DEPRECATE)

For CREATE:

* en parameter, som peger på det XML CDA dokument, der skal sende
* en valgfri entryUUid, som dokumentet skal gemmes med. Senere kan dokumentet rettes eller slettes vha. dette id. Angives ikke et entryUUid tildeles et. (Et entryUUid skal have prefix "urn:uuid:")

For REPLACE:

* entryUUid for det dokument der skal erstatte

For DEPRECATE:

* entryUUid for det dokument der skal deprecates
* cpr nummer


Derudover skal application.properties være på class path'en ved kørsel. Den skal indeholde de ovenstående angivne parametre/properties. Den enkelte parameter kan være blank i værdi, hvis den ikke giver mening for funktionen (se søjlen "Krævet" ovenfor))   

Et eksempel på application.properties findes i cda-ihe-xds-dgws-service-integrationtest/src/test/resources/application_ok.properties

Eksempler på kørsel og parametre: 

* Opret uden selvvalgt entryuuid
   * java -jar cda-ihe-xds-dgws-service-as-file-1.0.0.jar CREATE /documents/PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml
* Opret med selvvalgt entryuuid
   * java -jar cda-ihe-xds-dgws-service-as-file-1.0.0.jar CREATE /documents/PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml urn:uuid:efa5d2bf-06d8-4e0d-a2c1-8c8c4d736b41
* Opdater uden selvvalgt entryuuid
   * java -jar cda-ihe-xds-dgws-service-as-file-1.0.0.jar REPLACE /documents/PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml urn:uuid:efa5d2bf-06d8-4e0d-a2c1-8c8c4d736b41
* Opdater med selvvalgt entryuuid
   * java -jar cda-ihe-xds-dgws-service-as-file-1.0.0.jar REPLACE /documents/PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml urn:uuid:efa5d2bf-06d8-4e0d-a2c1-8c8c4d736b41 urn:uuid:efa5d2bf-06d8-4e0d-a2c1-000000000002
* Deprecate
   * java -jar cda-ihe-xds-dgws-service-as-file-1.0.0.jar DEPRECATE urn:uuid:efa5d2bf-06d8-4e0d-a2c1-8c8c4d736b41 123-cpr-90

Ved kørsel afsluttes med status kode:

* 0: alt gik godt
* 1: der var fejl. Kig  i loggen for yderligere detaljer


| Exception                    | Beskrivelse                                                             | 
|------------------------------|-------------------------------------------------------------------------| 
| XdsException                 | Ved fejl i forbindelse med kald til XDS servicen. Det kan være problemer med forbindelse til iti-41 eller STS endpoint. Eller hvis XDS servicen returnere fejl      |
| ParameterException           | Ved fejl i medsendte parametre. Det kan være funktionskode er ugyldig, stien til CDA dokumentet er forkert, eller manglende eller forkerte værdier i application.properties                   |

Log Konfigureres med logback

Efter kørsel kan dokumenter i test findes med Medcoms Cda Viewer (kræver login, som kan fåes hos Medcom):

* https://cdaviewer.medcom.dk/cdaviewer/ (vælg test 2 for KIH)
* søg med Patient id og unique id

### Anvendelse af metadata
Ved kald med CREATE og REPLACE fremfindes og anvendes metadata i forbindelse med registrering af dokumentet. Det foregår på følgende måde:

1. Faste værdier. F.eks AvailabilityStatus approved og type stable
1. Værdier fra property filen. F.eks. classcode og healthcarefacilitytypecode
1. Værdier udpakket af CDA builder/parser fra selve dokumentet. F.eks. author og patient. 
1. Andre værdier:
    - Når XML dokumentet er en QRD (typecode i dokumentet er 74465-6) tilføjes en eventcode. Den oprettes med data fra den første reference, der findes, som opfylder følgende kriterie (termer taget fra CDA builder/parser): referencen er af typen ExternalDocument og har ikke en ReferenceUse. Se eksempel i cda-ihe-xds-dgws-service-service/src/test/resources/QRD_Example_3.0_Text_Question_adj.xml


### Trin for trin test eksempel for CREATE. 
Det følgende skal ses som en hjælp/ en opskrift på, at få samlet de dele, som er beskrevet tidligere, for at kunne køre servicen, så man ikke skal starte helt fra bunden.

Certifikat og xml fil bør skiftes ud med egne filer.

Bygge

1. git clone https://<userid>@bitbucket.org/4s/cda-ihe-xds-dgws-service.git
1. cd cda-ihe-xds-dgws-service
1. mvn clean install

Køre

1. opret tomt bibliotek "cdaservice" på disk uden for git projektet
1. kopier cda-ihe-xds-dgws-service-as-file/target/cda-ihe-xds-dgws-service-as-file-x.y.z.jar til "cdaservice"
1. kopier cda-ihe-xds-dgws-service-service/src/test/resources/application.properties til "cdaservice"
1. kopier cda-ihe-xds-dgws-service-service/src/test/resources/NSP_Test_Service_Consumer_sds.p12 til "cdaservice"
1. kopier cda-ihe-xds-dgws-service-service/src/test/resources/PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml til "cdaservice"
1. kopier cda-ihe-xds-dgws-service-as-file-container/docker/config/template/logback-spring.xml til "cdaservice"

1. ret cdaservice/application.properties: fjern # fra linien #logging.config=logback-spring.xml
1. ret cdaservice/PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml: Linie 9 med <id extension. extension skal have en unik værdi    

1. i "cdaservice": java -jar cda-ihe-xds-dgws-service-as-file-x.y.z.jar CREATE PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text.xml

Resultatet i kommando prompten ved successfuld kørsel er oplysninger om dokumentid og entryuuid. Eks:

* A new document is registered with DocumentId = 1.2.208.184^437f8b60-2121-11e3-0001-00000000018 and entryUUid = urn:uuid:f784622b-d733-45de-95a2-5d4cf0d5ace3

## Info til udviklere

### Kildekodens afhængigheder
Kildekoden er afhængig af medcoms cda builder/parser. Den anvendes til at udtrække metadata og dokument data fra det indkomne dokument.

Cda builder/parseren findes her: https://bitbucket.org/4s/4s-cda-builder

cda-ihe-xds-dgws-service-as-file-container.Application og cda-ihe-xds-dgws-service-as-file.Application klasserne skal være ens.

### Kildekodens opbygning

Projektet er et maven projekt, og formålet med modulerne er:

* 4s-cda-builder-dependency: henter medcoms cda builder parser kode
* cda-ihe-xds-dgws-service-as-file: bygger jar fil, der kan kaldes 
* cda-ihe-xds-dgws-service-as-file-container: bygger jar fil, og pakker den ind i et docker image
* cda-ihe-xds-dgws-service-integrationstest: kører integrations test vha. container fra -as-file-container modulet
* cda-ihe-xds-dgws-service-service: forretnings logikken

### Bygge
Der bygges med maven:

 * mvn clean install

### Test

 
#### Integration test
Integration testen anvender Docker, så dette skal være installeret for at kunne køre denne. Dette muliggør blandt andet lettere skift mellem java versioner i tilfælde af problemer.
Testene kører over varianter af certifikater, properties og dokumenter, der alle findes i "src/test/resource".
Testen kører default mod kih respository, som findes på kih.test.xdsrepositoryb.medcom.dk
Under testkørslen oprettes, erstattes og deprecates dokumenter i det valgte repository.

Forbered testen

 * hvis endpoint skal ændres i forhold til standard, rettes alle properties filer i biblioteket "src/test/resources/properties"

Kørsel af testen

  * Kør maven profilen "integration-test": mvn clean install -Pintegration-test

Denne bygger et docker image og starter integrations testen op. Integrations testen kører vha. test-containers 


### Release
Ved ændringer til source koden udføres ændres pom filernes versions nummmer  (husk at notere ændringerne per version i relase notes
 
### Anden relevant information



## Release notes
  
| Version (pom) | Ændring                                                                                 | 
|---------------|-----------------------------------------------------------------------------------------| 
| 1.1.0         | Ændret på, hvornår authorInstitution mappes fra dokumentet til registreringsdata. Udover de gamle regler, så oprettes nu kun en Author.AuthorInstitution, hvis <name> på <representedOrganization> er sat.   |
| 1.1.0         | Ændret application.properties eksempel classcode.name fra Klinisk rapport til Clinical report   |
| 1.2.0         | Ændret version af spring boot framework, så log4j version opdateres (CVE-2021-44228) |
| 1.3.0         | Ændret på, hvordan author id i XML mappes til AuthorInstitution code i metadata. Hvis assignedAuthor.id er CPR (root="1.2.208.176.1.2") og representedOrganization.id er SOR (root="1.2.208.176.1.1) tages værdien fra representedOrganization istedet for assignedAuthor|
|               | Ændret xds.iti57.endpoint i alle properties filer til at anvende DROS istedet for DDS registry. DDS endpointet udgår |
|               | Ændret sådan at metadata af typen LocalizedString nu oprettes med sprog da-DK istedet for en-US |
| 1.4.0         | Ændret oprettelse af metadata. Hvis QRD kigges efter en reference. Denne reference registreres som eventCode i dokumentets metadata  |
| 1.5.0         | Udskiftet test certifikat til et som ikke er udløbet  |
|               | Tilføjet HomeCommunityId til properties. Værdi skal angives og afhænger af de endpoints der anvendes. Se eksempel i application.properties  |
| 1.6.0         | Opgradering af Medcom Cda Builder Parser fra 6.2.3 til 6.3.3  |
| 1.6.1         | Opgradering af Medcom Cda Builder Parser fra 6.3.3 til 6.3.4  |
| 1.7.0         | Anvend tagget version af Medcom Cda Builder Parser  |
|               | Anvendelse af oces3 test certifikat samt opdatering af nsp dependencies |
| 1.7.1         | Justering af iti-41 endpoint i properties filerne og nyere base image til integrationstesten |