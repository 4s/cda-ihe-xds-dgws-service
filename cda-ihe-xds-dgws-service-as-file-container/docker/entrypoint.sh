#! /bin/ash
if [ "$CONTAINER_TIMEZONE" = "" ]
then
   echo "Using default timezone"
else
	
	TZFILE="/usr/share/zoneinfo/$CONTAINER_TIMEZONE"
	if [ ! -e "$TZFILE" ]
	then 
    	echo "requested timezone $CONTAINER_TIMEZONE doesn't exist"
	else
		cp /usr/share/zoneinfo/$CONTAINER_TIMEZONE /etc/localtime
		echo "$CONTAINER_TIMEZONE" > /etc/timezone
		echo "using timezone $CONTAINER_TIMEZONE"
	fi
fi

if [[ -z $LOG_LEVEL ]]; then
  echo "Default LOG_LEVEL = INFO"
  export LOG_LEVEL=INFO
fi

if [[ -z $CORRELATION_ID ]]; then
  echo "Default CORRELATION_ID = correlation-id"
  export CORRELATION_ID=correlation-id
fi

if [[ -z $SERVICE_ID ]]; then
  echo "Default SERVICE_ID = cda-ihe-xds-dgws-service"
  export SERVICE_ID=cda-ihe-xds-dgws-service
fi

#envsubst < /config/template/application.properties > /app/application.properties
envsubst < /config/template/logback-spring.xml > /app/logback-spring.xml
#envsubst < /config/template/test.jks > /app/test.jks

echo "arguments: $1 $2 $3 $4"

java $JVM_OPTS -jar cda-ihe-xds-dgws-service-as-file.jar $1 $2 $3 $4
#/bin/ash
