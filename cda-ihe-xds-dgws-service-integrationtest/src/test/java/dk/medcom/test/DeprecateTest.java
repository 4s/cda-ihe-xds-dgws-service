package dk.medcom.test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import dk.medcom.document.DocumentFactoryPHMR;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;

public class DeprecateTest extends IntegrationTestContainers {
	
	DocumentFactoryPHMR documentFactoryPhmr = new DocumentFactoryPHMR();
	
	
	@Rule public TestName name = new TestName();
	private String containerNameCreate;
	private String containerNameDeprecate;

	@Before
	public void setContainerName() {
		containerNameCreate = name.getMethodName() + "Create";
		containerNameDeprecate = name.getMethodName() + "Deprecate";
	}

	@Test
	public void testAllOK() throws IOException {

		// Given
		String propertyResource = "properties/application_ok.properties";
		String entryUuidToDeprecate = createDocumentToDeprecate(propertyResource);
		
		// When
		String log = triggerDeprecateDocument(propertyResource, containerNameDeprecate, entryUuidToDeprecate, cpr);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 0"));
		assertTrue(log.contains("A document is deprecated with entryUUid = " + entryUuidToDeprecate));
		
	}
	
	@Test
	public void testWithOutEntryUuidToDeprecate() throws IOException {

		// Given
		String propertyResource = "properties/application_ok.properties";
		
		// When
		String log = triggerDeprecateDocument(propertyResource, containerNameDeprecate, null, cpr);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 1"));
		assertTrue(log.contains("cpr is not available")); //it thinks cpr is entryuuid and is then missing cpr

		
	}
	
	@Test
	public void testWithNonExisitingEntryUuidToDeprecate() throws IOException {

		// Given
		String propertyResource = "properties/application_ok.properties";
		String entryUuidToReplace = "urn:uuid:" + java.util.UUID.randomUUID().toString();
		
		// When
		String log = triggerDeprecateDocument(propertyResource, containerNameDeprecate, entryUuidToReplace, cpr);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 1"));
		assertTrue(log.contains("The XDS reported errors"));
		assertTrue(log.contains("contains an illegal target reference: " + entryUuidToReplace));
		
	}
	
	@Test
	public void testWithOutCprToDeprecate() throws IOException {

		// Given
		String propertyResource = "properties/application_ok.properties";
		String entryUuidToDeprecate = createDocumentToDeprecate(propertyResource);
		
		// When
		String log = triggerDeprecateDocument(propertyResource, containerNameDeprecate, entryUuidToDeprecate, null);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 1"));
		assertTrue(log.contains("cpr is not available"));
		
	}
	
	@Test
	public void testWithOutRepositoryUniqueIdMissing() throws IOException {

		// Given
		String propertyResource = "properties/application_ok.properties";
		String entryUuidToDeprecate = createDocumentToDeprecate(propertyResource);
		
		// When
		propertyResource = "properties/application_missing_repositoryuniqueid_value.properties";
		String log = triggerDeprecateDocument(propertyResource, containerNameDeprecate, entryUuidToDeprecate, cpr);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 1"));
		assertTrue(log.contains("xds.repositoryuniqueid is not available"));
		
	}
	
	@Test
	public void testWithOutTypeCodeNameMissing() throws IOException {

		// Given
		String propertyResource = "properties/application_ok.properties";
		String entryUuidToDeprecate = createDocumentToDeprecate(propertyResource);
		
		// When
		propertyResource = "properties/application_missing_type_code_name_value.properties";	
		String log = triggerDeprecateDocument(propertyResource, containerNameDeprecate, entryUuidToDeprecate, cpr);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 1"));
		assertTrue(log.contains("typecode.name is not available"));
		
	}
	
	private String createDocumentToDeprecate(String propertyResource) throws IOException {
		
		PHMRDocument phmrDocument = documentFactoryPhmr.defineAsCDA(java.util.UUID.randomUUID().toString(), cpr);
		ID docId = documentToFile(phmrDocument);

		String documentLink = "/documents/" +   docId.getExtension() + ".xml";
		String entryUuidToReplace = "urn:uuid:" + java.util.UUID.randomUUID().toString();

		String log = triggerCreateDocument(propertyResource, containerNameCreate, documentLink, entryUuidToReplace);
		
		assertTrue(log.contains("Ending application with status code 0"));
		return entryUuidToReplace;
	}
	

}
