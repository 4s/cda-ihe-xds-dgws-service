package dk.medcom.test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import dk.medcom.document.DocumentFactoryPHMR;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;

public class CreateTest extends IntegrationTestContainers {
	
	DocumentFactoryPHMR documentFactoryPhmr = new DocumentFactoryPHMR();
	
	@Rule public TestName name = new TestName();
	private String containerName;

	@Before
	public void setContainerName() {
		containerName = name.getMethodName();
	}
		
	@Test
	public void testAllOKNoEntryUuid() throws IOException {

		// Given
		PHMRDocument phmrDocument = documentFactoryPhmr.defineAsCDA(java.util.UUID.randomUUID().toString(), cpr);
		ID docId = documentToFile(phmrDocument);

		String propertyResource = "properties/application_ok.properties";
		String documentLink = "/documents/" +   docId.getExtension() + ".xml";
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, null);
		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 0"));
		assertTrue(log.contains("A new document is registered with DocumentId = " + docId.getRoot() + "^" + docId.getExtension()));
		
	}
	
	@Test
	public void testAllOKEntryUuid() throws IOException {

		// Given
		PHMRDocument phmrDocument = documentFactoryPhmr.defineAsCDA(java.util.UUID.randomUUID().toString(), cpr);
		ID docId = documentToFile(phmrDocument);

		String propertyResource = "properties/application_ok.properties";
		String documentLink = "/documents/" +   docId.getExtension() + ".xml";
		String entryUuid = "urn:uuid:" + java.util.UUID.randomUUID().toString();
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, entryUuid);
		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 0"));
		assertTrue(log.contains("A new document is registered with DocumentId = " + docId.getRoot() + "^" + docId.getExtension() + " and entryUUid = " + entryUuid));
		
	}
	
	@Test
	public void testNoDocumentGiven() throws IOException {
		
		// Given
		String propertyResource = "properties/application_ok.properties";
		String documentLink = "";
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, null);
		
		// Then
		assertTrue(log.contains("CDA filename is not available"));
		
	}

	@Test
	public void testMissingAuthorInDocumentOK() throws IOException {
		
		// Given
		PHMRDocument phmrDocument = documentFactoryPhmr.defineAsCDA(java.util.UUID.randomUUID().toString().substring(0,15), cpr);
		phmrDocument.setAuthor(null);
		ID docId = documentToFile(phmrDocument);
		
		String propertyResource = "properties/application_ok.properties";
		String documentLink = "/documents/" +   docId.getExtension() + ".xml";
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, null);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 1"));
		assertTrue(log.contains("The XDS reported errors"));
		
	}

	@Test
	public void testInvalidXMLSyntaxInDocumentOK() throws IOException {
		
		// Given
		String propertyResource = "properties/application_ok.properties";
		String documentLink = "/documents/PHMR_Test3_invalidXMLSyntax.xml";
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, null);
		
		// Then
		assertTrue(log.contains("Ending application with status code 1"));
		assertTrue(log.contains("Cant parse xml document Unexpected close tag </ClinicalDocument>; expected </author>."));
		
	}

	@Test
	public void testEmptyPropertiesGiven() throws IOException {
		
		// Given
		String propertyResource = "properties/application_empty.properties";
		String documentLink = "/documents/PHMR_Test4_neverSent.xml";
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, null);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Unsatisfied dependency expressed through"));
		
	}

	@Test
	public void testMissingClassCodeNameValue() throws IOException {
		
		// Given
		String propertyResource = "properties/application_missing_class_code_name_value.properties";
		String documentLink = "/documents/PHMR_Test4_neverSent.xml";
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, null);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 1"));
		assertTrue(log.contains("classcode.name is not available"));
		
	}

	@Test
	public void testInvalidIti41Endpoint() throws IOException {
		
		// Given
		String propertyResource = "properties/application_invalid_iti41_endpoint.properties";
		String documentLink = "/documents/PHMR_Test4_neverSent.xml";
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, null);
//		System.out.println(log);
		
		// Then
	    assertTrue(log.contains("XdsException: Error calling externally"));
		assertTrue(log.contains("Could not send Message"));
		
	}

	@Test
	public void testNonExistingServerSTSEndpointNoEntryUuid() throws IOException {
		
		// Given
		String propertyResource = "properties/application_nonexisting_server_sts_endpoint.properties";
		String documentLink = "/documents/PHMR_Test4_neverSent.xml";
	
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, null);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("XdsException: Error calling externally"));
		assertTrue(log.contains("DgwsSoapDecorator"));
		
	}

	@Ignore("Pt findes ikke noget udløbet oces3 certifikat") //TODO: enables igen, når oces3 udløbet certifikat findes.
	@Test
	public void testCertificateExpiredNoEntryUuid() throws IOException {
		
		// Given
		String propertyResource = "properties/application_cert_expir.properties";
		String documentLink = "/documents/PHMR_Test4_neverSent.xml";
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, null);
		
		// Then
		assertTrue(log.contains("certificate expired"));
		
	}

	

	@Ignore //Do not delete, might be relevant to test from time to time
	@Test
	public void testAllOKTest1ServerNoEntryUuid() throws IOException {
		
		// Given
		PHMRDocument phmrDocument = documentFactoryPhmr.defineAsCDA(java.util.UUID.randomUUID().toString(), cpr); 
		ID docId = documentToFile(phmrDocument);
		
		String propertyResource = "properties/application_ok_test1.properties";
		String documentLink = "/documents/" +   docId.getExtension() + ".xml";
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, null);
		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 0"));
		
	}
	
	@Ignore //Do not delete, might be relevant if we at some point needs to test a test file provided
	@Test
	public void testAllOK_fromFileNoEntryUuid() throws IOException {
		
		// Given
		String patientIdToFind = "¤PATIENTID¤";
		String documentIdToFind = "¤DOCUMENTID¤";
		String patientIdToreplaceWith = "2512489996";
		String documentIdToreplaceWith = java.util.UUID.randomUUID().toString();
		String documentName = "doc" + documentIdToreplaceWith +".xml";
		String fileNameFrom = "documents/PHMR_KOL_Example_4_MaTIS_Validated_Devices_Reference_RED_YELLOW_Text_template.xml";
		
		String fromFilePath = this.getClass().getClassLoader().getResource(fileNameFrom).getFile();
		Path fromFile = Paths.get(fromFilePath);
		String toFilePath = "target/test-classes/documents/" + documentName;
		Path toFile = Paths.get(toFilePath);

	    Files.copy(fromFile, toFile, StandardCopyOption.REPLACE_EXISTING);
		
	    readReplaceWrite(toFilePath, patientIdToFind,patientIdToreplaceWith);
	    readReplaceWrite(toFilePath, documentIdToFind,documentIdToreplaceWith);
		    
		String propertyResource = "properties/application_ok.properties";
		String documentLink = "/documents/" +   documentName;
		
		// When
		String log = triggerCreateDocument(propertyResource, containerName, documentLink, null);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 0"));
		assertTrue(log.contains("A new document is registered with DocumentId = 1.2.208.184^" + documentIdToreplaceWith));
		
	}
	
	private void readReplaceWrite(String filePath, String stringToFind, String stringToreplaceWith) throws IOException {
		Path path = Paths.get(filePath);
		Stream <String> lines = Files.lines(path);
		List <String> replaced = lines.map(line -> line.replaceAll(stringToFind, stringToreplaceWith)).collect(Collectors.toList());
		Files.write(path, replaced);
	}

	

}
