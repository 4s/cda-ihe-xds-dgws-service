package dk.medcom.test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import dk.medcom.document.DocumentFactoryPHMR;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;

public class ReplaceTest extends IntegrationTestContainers {
	
	DocumentFactoryPHMR documentFactoryPhmr = new DocumentFactoryPHMR();
	
	
	@Rule public TestName name = new TestName();
	private String containerNameCreate;
	private String containerNameReplace;

	@Before
	public void setContainerName() {
		containerNameCreate = name.getMethodName() + "Create";
		containerNameReplace = name.getMethodName() + "Replace";
	}
	
	@Test
	public void testAllOKNoEntryUuid() throws IOException {

		// Given
		String propertyResource = "properties/application_ok.properties";
		String entryUuidToReplace = createDocumentToReplace(propertyResource);
		
		PHMRDocument phmrDocument = documentFactoryPhmr.defineAsCDA(java.util.UUID.randomUUID().toString(), cpr);
		ID docId = documentToFile(phmrDocument);
		String documentLink = "/documents/" +   docId.getExtension() + ".xml";
		
		// When
		String log = triggerReplaceDocument(propertyResource, containerNameReplace, documentLink, entryUuidToReplace, null);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 0"));
		assertTrue(log.contains("A document is registered with DocumentId = " + docId.getRoot() + "^" + docId.getExtension()));
		assertTrue(log.contains("as a replacement of document with entryUUid = " + entryUuidToReplace));
		
		
	}
	
	@Test
	public void testAllOKEntryUuid() throws IOException {

		// Given
		String propertyResource = "properties/application_ok.properties";
		String entryUuidToReplace = createDocumentToReplace(propertyResource);
		
		PHMRDocument phmrDocument = documentFactoryPhmr.defineAsCDA(java.util.UUID.randomUUID().toString(), cpr);
		ID docId = documentToFile(phmrDocument);
		String documentLink = "/documents/" +   docId.getExtension() + ".xml";	
		String entryUuid = "urn:uuid:" + java.util.UUID.randomUUID().toString();
		
		// When
		String log = triggerReplaceDocument(propertyResource, containerNameReplace, documentLink, entryUuidToReplace, entryUuid);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 0"));
		assertTrue(log.contains("A document is registered with DocumentId = " + docId.getRoot() + "^" + docId.getExtension() + " and entryUUid = " + entryUuid));
		assertTrue(log.contains("as a replacement of document with entryUUid = " + entryUuidToReplace));
		
	}
	
	@Test
	public void testWithOutEntryUuidToReplace() throws IOException {

		// Given
		String propertyResource = "properties/application_ok.properties";
		
		PHMRDocument phmrDocument = documentFactoryPhmr.defineAsCDA(java.util.UUID.randomUUID().toString(), cpr);
		ID docId = documentToFile(phmrDocument);
		String documentLink = "/documents/" +   docId.getExtension() + ".xml";	
		
		// When
		String log = triggerReplaceDocument(propertyResource, containerNameReplace, documentLink, null, null);
//		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 1"));
		assertTrue(log.contains("entryUuid to replace is not available"));

		
	}
	
	@Test
	public void testWithNonExisitingEntryUuidToReplace() throws IOException {

		// Given
		String propertyResource = "properties/application_ok.properties";
		String entryUuidToReplace = "urn:uuid:" + java.util.UUID.randomUUID().toString();
		
		PHMRDocument phmrDocument = documentFactoryPhmr.defineAsCDA(java.util.UUID.randomUUID().toString(), cpr);
		ID docId = documentToFile(phmrDocument);
		String documentLink = "/documents/" +   docId.getExtension() + ".xml";	
		
		// When
		String log = triggerReplaceDocument(propertyResource, containerNameReplace, documentLink, entryUuidToReplace, null);
		System.out.println(log);
		
		// Then
		assertTrue(log.contains("Ending application with status code 1"));
		assertTrue(log.contains("The XDS reported errors"));
		assertTrue(log.contains("A DocumentEntry with the specified entryuuid '" + entryUuidToReplace + "' does not exist in the registry"));
		
	}
	
	private String createDocumentToReplace(String propertyResource) throws IOException {
		
		PHMRDocument phmrDocument = documentFactoryPhmr.defineAsCDA(java.util.UUID.randomUUID().toString(), cpr);
		ID docId = documentToFile(phmrDocument);

		String documentLink = "/documents/" +   docId.getExtension() + ".xml";
		String entryUuidToReplace = "urn:uuid:" + java.util.UUID.randomUUID().toString();

		String log = triggerCreateDocument(propertyResource, containerNameCreate, documentLink, entryUuidToReplace);
		
		assertTrue(log.contains("Ending application with status code 0"));
		return entryUuidToReplace;
	}
	

}
