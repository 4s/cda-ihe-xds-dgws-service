package dk.medcom.test;

import java.io.IOException;
import java.util.function.Consumer;

import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.OutputFrame.OutputType;
import org.testcontainers.containers.output.ToStringConsumer;

import com.github.dockerjava.api.command.CreateContainerCmd;

import dk.medcom.testcontainers.IndefiniteWaitOneShotStartupCheckExitCodeStrategy;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;


public class IntegrationTestContainers {
	
	public static final String CDA_SERVICE_IMAGE = "medcom/cda-ihe-xds-dgws-service:latest";
	public static final String CDA_SERVICE_ALIAS = "cda-ihe-xds-dgws-service";
	
	public static final String LOG_FILES_PATH = "integrationtestlogs";
	
	protected String cpr = "2512489996";

	public String triggerCreateDocument(String propertyResource, String containerName, String documentLink, String entryUUid) {
		
		String[] commands;
		if (entryUUid != null) {
			commands = new String[3];
			commands[0] = "CREATE";
			commands[1] = documentLink;			
			commands[2] = entryUUid;
		} else {
			commands = new String[2];
			commands[0] = "CREATE";
			commands[1] = documentLink;
		}

		return startUploadDocument(propertyResource, containerName, commands);
		
	}
	
	public String triggerReplaceDocument(String propertyResource, String containerName, String documentLink, String entryUUidToReplace, String entryUUid) {
		
		String[] commands;
		if (entryUUid != null) {
			commands = new String[4];
			commands[0] = "REPLACE";
			commands[1] = documentLink;			
			commands[2] = entryUUidToReplace;
			commands[3] = entryUUid;
		} else {
			commands = new String[3];
			commands[0] = "REPLACE";
			commands[1] = documentLink;
			commands[2] = entryUUidToReplace;
		}

		return startUploadDocument(propertyResource, containerName, commands);
		
	}
	
	public String triggerDeprecateDocument(String propertyResource, String containerName, String entryUUidToDeprecate, String cpr) {
		
		String[] commands;
		commands = new String[3];
		commands[0] = "DEPRECATE";
		commands[1] = entryUUidToDeprecate;
		commands[2] = cpr;

		return startDeprecateDocument(propertyResource, containerName, commands);
		
	}
	
	private String startUploadDocument(String propertyResource, String containerName, String[] commands) {
		
		GenericContainer<?> cdaService = new GenericContainer<>(CDA_SERVICE_IMAGE)
				.withNetworkMode("host")
				.withNetworkAliases(CDA_SERVICE_ALIAS)
		        .withCreateContainerCmdModifier(new Consumer<CreateContainerCmd>() {
		        	@Override
		        	public void accept(CreateContainerCmd createContainerCmd) {
		        		createContainerCmd.withName(containerName);
		        		}
		        	})
		        .withCommand(commands)
		        .withClasspathResourceMapping(propertyResource, "/app/application.properties", BindMode.READ_ONLY)
		        .withClasspathResourceMapping("certificates/NSP_Test_Service_Consumer_sds.p12", "/app/NSP_Test_Service_Consumer_sds.p12", BindMode.READ_ONLY)
//		        .withClasspathResourceMapping("certificates/FOCES_udloebet.p12", "/app/FOCES_udloebet.p12", BindMode.READ_ONLY) //TODO: enables igen, når oces3 udløbet certifikat findes.
		        .withClasspathResourceMapping("documents", "/documents", BindMode.READ_ONLY)
		        .withStartupCheckStrategy(new IndefiniteWaitOneShotStartupCheckExitCodeStrategy())
		        ;
		
		ToStringConsumer toStringConsumer = new ToStringConsumer();
		cdaService.start();
		
		cdaService.followOutput(toStringConsumer, OutputType.STDOUT);
		cdaService.close();
		String utf8String = toStringConsumer.toUtf8String();

		return utf8String;
		
	}
	
	private String startDeprecateDocument(String propertyResource, String containerName, String[] commands) {
		
		GenericContainer<?> cdaService = new GenericContainer<>(CDA_SERVICE_IMAGE)
				.withNetworkMode("host")
				.withNetworkAliases(CDA_SERVICE_ALIAS)
		        .withCreateContainerCmdModifier(new Consumer<CreateContainerCmd>() {
		        	@Override
		        	public void accept(CreateContainerCmd createContainerCmd) {
		        		createContainerCmd.withName(containerName);
		        		}
		        	})
		        .withCommand(commands)
		        .withClasspathResourceMapping(propertyResource, "/app/application.properties", BindMode.READ_ONLY)
		        .withClasspathResourceMapping("certificates/NSP_Test_Service_Consumer_sds.p12", "/app/NSP_Test_Service_Consumer_sds.p12", BindMode.READ_ONLY)
//		        .withClasspathResourceMapping("certificates/FOCES_udloebet.p12", "/app/FOCES_udloebet.p12", BindMode.READ_ONLY) //TODO: enables igen, når oces3 udløbet certifikat findes.
		        .withStartupCheckStrategy(new IndefiniteWaitOneShotStartupCheckExitCodeStrategy())
		        ;
		
		ToStringConsumer toStringConsumer = new ToStringConsumer();
		cdaService.start();
		
		cdaService.followOutput(toStringConsumer, OutputType.STDOUT);
		cdaService.close();
		String utf8String = toStringConsumer.toUtf8String();

		return utf8String;
		
	}
	
	protected ID documentToFile(PHMRDocument phmrDocument) throws IOException {

		PHMRXmlCodec codec = new PHMRXmlCodec();

		String xmlDocument = codec.encode(phmrDocument);
		String docName = phmrDocument.getId().getExtension();

		String tmpName = "target/test-classes/documents/" + docName + ".xml";

		java.io.FileWriter fw = new java.io.FileWriter(tmpName);
		fw.write(xmlDocument);
		fw.close();

		return phmrDocument.getId();

	}


}
