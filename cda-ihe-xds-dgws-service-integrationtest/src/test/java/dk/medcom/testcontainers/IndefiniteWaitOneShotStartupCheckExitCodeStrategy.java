package dk.medcom.testcontainers;

import java.util.concurrent.TimeUnit;

import org.testcontainers.containers.startupcheck.OneShotStartupCheckStrategy;
import org.testcontainers.shaded.com.google.common.util.concurrent.Uninterruptibles;
import org.testcontainers.utility.DockerStatus;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.InspectContainerResponse;

public class IndefiniteWaitOneShotStartupCheckExitCodeStrategy extends OneShotStartupCheckStrategy {

    @Override
    public boolean waitUntilStartupSuccessful(DockerClient dockerClient, String containerId) {
        while (checkStartupState(dockerClient, containerId) == StartupStatus.NOT_YET_KNOWN) {
            Uninterruptibles.sleepUninterruptibly(100, TimeUnit.MILLISECONDS);
        }
        
        
        InspectContainerResponse.ContainerState state = getCurrentState(dockerClient, containerId);
        
        if (DockerStatus.isContainerStopped(state) && (isContainerExitCode0(state) || isContainerExitCode1(state))) {
        	return true;
        }
        return false;

    }
    
    private boolean isContainerExitCode0(InspectContainerResponse.ContainerState state) {
      int exitCode = state.getExitCode();
      // 0 is the only exit code we can consider as success
      return exitCode == 0;
    }
    
    private boolean isContainerExitCode1(InspectContainerResponse.ContainerState state) {
        int exitCode = state.getExitCode();
        // 0 is the only exit code we can consider as success
        return exitCode == 1;
      }
    

	
}
